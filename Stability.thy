theory Stability
  imports Predicates
begin

type_synonym ('r,'v,'s) prel = "('r \<times> ('r,'v,'s) pred \<times> ('v \<times> 's) rel) set"

section \<open>Stability\<close>

text \<open>Condition under which two memories are related by a @{type prel}\<close>
abbreviation rstep
  where "rstep R m m' \<equiv> \<forall>xa\<in>R. case xa of (xa, c, r) \<Rightarrow> m \<in> c \<longrightarrow> (m xa, m' xa) \<in> r\<^sup>*"

text \<open>Extend @{term rstep} to a relation on memories\<close>
abbreviation prel
  where "prel R \<equiv> {(m,m'). rstep R m m'}"

text \<open>Stability of a predicate across an environment step\<close>
definition stable :: "('b,'c,'d) prel \<Rightarrow> ('b,'c,'d) pred \<Rightarrow> bool"
  where "stable R P \<equiv> \<forall>m \<in> P. \<forall>m'. (m,m') \<in> prel R \<longrightarrow> m' \<in> P"

subsection \<open>Transitive\<close>

text \<open>
Condition under which a  @{type prel} is considered transitive.
Effectively, no transition can invalidate any variable's condition.
\<close>
definition transitive :: "('r,'v,'s) prel \<Rightarrow> bool"
  where "transitive R \<equiv> \<forall>(x,c,r) \<in> R. \<forall>m \<in> c. \<forall>m'. (m,m') \<in> prel R \<longrightarrow> m' \<in> c"

lemma rstep_transitive:
  assumes "transitive R" "rstep R m\<^sub>1 m\<^sub>2" "rstep R m\<^sub>2 m\<^sub>3"
  shows "rstep R m\<^sub>1 m\<^sub>3"
proof (clarsimp simp: trans_def)
  fix x c r assume a: "(x,c,r) \<in> R" "m\<^sub>1 \<in> c" 
  have "m\<^sub>2 \<in> c" using assms a unfolding transitive_def by auto
  hence "(m\<^sub>2 x, m\<^sub>3 x) \<in> r\<^sup>*" using assms a by auto
  moreover have "(m\<^sub>1 x, m\<^sub>2 x) \<in> r\<^sup>*" using assms a by auto
  ultimately show "(m\<^sub>1 x, m\<^sub>3 x) \<in> r\<^sup>*" by auto
qed

text \<open>
Proof that the transitive property on @{type prel} implies the computed memory relation 
is transitive.\<close>
lemma prel_transitive:
  assumes "transitive R"
  shows "trans (prel R)"
  using rstep_transitive[OF assms] by (clarsimp simp: trans_def)

subsection \<open>Reflexive\<close>

text \<open>A @{type prel} is trivially reflexive\<close>
lemma prel_reflexive:
  shows "refl (prel R)"
  by (auto simp: refl_on_def)

subsection \<open>Predicate Manipulation\<close>

lemma stable_inter [intro]:
  assumes "stable \<L> P" "stable \<L> Q"
  shows "stable \<L> (P \<inter> Q)"
  using assms unfolding stable_def by auto

end