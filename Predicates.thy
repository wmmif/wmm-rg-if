theory Predicates
  imports Main
begin

chapter \<open>State and Predicate Encoding\<close>

text \<open>\<close>

type_synonym ('r,'v,'s) mem = "'r \<Rightarrow> 'v \<times> 's"
type_synonym ('r,'v,'s) pred = "('r,'v,'s) mem set"

section \<open>Partial Equivalence\<close>

definition partial_equiv :: "('r \<Rightarrow> 'v) \<Rightarrow> 'r set \<Rightarrow> ('r \<Rightarrow> 'v) \<Rightarrow> bool"("_ \<approx>_\<approx> _" [100,0,100] 60)
  where "partial_equiv m S m' \<equiv> \<forall>x \<in> S. m x = m' x"

lemma partial_equiv_sub:
  assumes "m \<approx>P\<approx> n" "Q \<subseteq> P"
  shows "m \<approx>Q\<approx> n"
  using assms by (auto simp: partial_equiv_def)

lemma partial_equiv_trans:
  assumes "m \<approx>P\<approx> n" "n \<approx>P\<approx> q" 
  shows "m \<approx>P\<approx> q"
  using assms by (auto simp: partial_equiv_def)

lemma partial_equiv_refl [intro]:
  "m \<approx>S\<approx> m"
  unfolding partial_equiv_def by auto

section \<open>Existential Quantification\<close>

definition restrict :: "('r,'v,'s) pred \<Rightarrow> 'r set \<Rightarrow> ('r,'v,'s) pred" (infix "\<downharpoonright>\<^sub>E" 60)
  where "restrict P S \<equiv> {m. \<exists>m' \<in> P. m \<approx>S\<approx> m'}"

lemma restrict_ord:
  "P \<subseteq> restrict P S"
  unfolding restrict_def by auto

lemma restrict_mono:
  assumes "S' \<subseteq> S"
  shows "restrict P S \<subseteq> restrict P S'"
  using assms unfolding restrict_def by (auto intro: partial_equiv_sub)

lemma restrict_conseq:
  assumes "P \<subseteq> P'" "S' \<subseteq> S" 
  shows "restrict P S \<subseteq> restrict P' S'"
  using assms unfolding restrict_def 
  by (auto intro: partial_equiv_sub)

section \<open>Relations\<close>

text \<open>Constrain the pre of a step\<close>
definition pre :: "('b,'c,'d) pred \<Rightarrow> ('b \<Rightarrow> 'c \<times> 'd) rel" ("\<lfloor>_\<rfloor>" 80)
  where "pre P \<equiv> {(a,b). a \<in> P}"

lemma pre_ord [intro]:
  assumes "P \<subseteq> Q"
  shows "x \<in> \<lfloor>P\<rfloor> \<Longrightarrow> x \<in> \<lfloor>Q\<rfloor>"
  using assms by (auto simp: pre_def)

text \<open>Preserve a state across a step\<close>
definition preserve :: "'a set \<Rightarrow> 'a rel"
  where "preserve P \<equiv> {(m,m'). (m) \<in> P \<longrightarrow> (m') \<in> P}"

end