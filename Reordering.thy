theory Reordering
  imports Syntax
begin

chapter \<open>Reordering Properties\<close> 

text \<open>
Assume an externally provided reordering and forwarding functions, to define the memory model.
The ideas here are derived from Colvin & Smith.
From these definitions we can recursively define reordering and forwarding over programs.
\<close>

locale reordering =
  fixes reorder :: "'a \<Rightarrow> 'a \<Rightarrow> 'a option"

context reordering
begin

text \<open>Abbreviation to simplify use of reordering condition\<close>
abbreviation reorder_inst :: "'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> bool"
  ("_ < _ <\<^sub>a _" [100,0,100] 100)
  where "e' < \<alpha> <\<^sub>a e \<equiv> reorder \<alpha> e = Some (e')"

abbreviation fwd :: "'a \<Rightarrow> 'a \<Rightarrow> 'a" 
  ("_\<langle>_\<rangle>" [100,100] 100)
  where "fwd \<alpha> \<beta> \<equiv> case reorder \<beta> \<alpha> of Some \<alpha>' \<Rightarrow> \<alpha>' | None \<Rightarrow> \<alpha>"

text \<open>Recursively define reordering of an instruction earlier than a program\<close>
fun reorder_com :: "'a \<Rightarrow> 'a com \<Rightarrow> 'a \<Rightarrow> bool"
  ("_ < _ <\<^sub>c  _" [100,0,100] 100)
  where
    "e' < Skip <\<^sub>c e = (e' = e)" |
    "e' < Basic \<alpha> <\<^sub>c e = (e' < \<alpha> <\<^sub>a e)" |
    "e' < c\<^sub>1 ;; c\<^sub>2 <\<^sub>c e = (\<exists>e\<^sub>n. e' < c\<^sub>1 <\<^sub>c e\<^sub>n \<and> e\<^sub>n < c\<^sub>2 <\<^sub>c e)" |
    "_ < _ <\<^sub>c _ = False"

end

end