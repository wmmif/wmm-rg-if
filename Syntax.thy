theory Syntax
  imports Main
begin

datatype 'a com =
  Skip
  | Basic "'a"
  | Seq "'a com" "'a com" (infixr ";;" 80)
  | Choice "'a com" "'a com" (infixr "\<sqinter>" 150)
  | Loop "'a com" ("_*" [100] 150)

end