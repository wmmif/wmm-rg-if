theory Trace
  imports Semantics
begin

locale trace = semantics +
  assumes beh_det: "(m,m') \<in> eval \<alpha> \<Longrightarrow> (m,m'') \<in> eval \<alpha> \<Longrightarrow> m' = m''"

context trace
begin

inductive_set trace :: "('a com \<times> 'a list \<times> 'a com) set"
  and trace_abv :: "'a com \<Rightarrow> 'a list \<Rightarrow> 'a com \<Rightarrow> bool" ("_ \<mapsto>_\<^sup>* _" [50,40,40] 70)
  where
  "trace_abv P t P' \<equiv> (P, t, P') \<in> trace"
  | lift[intro]:    "c \<mapsto>([])\<^sup>* c"
  | rewrite[intro]: "c\<^sub>1 \<leadsto> c\<^sub>2 \<Longrightarrow> c\<^sub>2 \<mapsto>t\<^sup>* c\<^sub>3 \<Longrightarrow> c\<^sub>1 \<mapsto>t\<^sup>* c\<^sub>3"
  | prepend[intro]: "c\<^sub>1 \<mapsto>[e',r,e] c\<^sub>2 \<Longrightarrow> c\<^sub>2 \<mapsto>t\<^sup>* c\<^sub>3 \<Longrightarrow> c\<^sub>1 \<mapsto>e'#t\<^sup>* c\<^sub>3"

lemma trace_concatE [elim]:
  assumes "P\<^sub>1 \<mapsto>t\<^sub>1@t\<^sub>2\<^sup>* P\<^sub>3"
  obtains P\<^sub>2 where "P\<^sub>1 \<mapsto>t\<^sub>1\<^sup>* P\<^sub>2" "P\<^sub>2 \<mapsto>t\<^sub>2\<^sup>* P\<^sub>3"
  using assms
proof (induct P\<^sub>1 "t\<^sub>1 @t\<^sub>2" P\<^sub>3 arbitrary: t\<^sub>1 t\<^sub>2)
  case (prepend c\<^sub>1 \<alpha> c\<^sub>2 t c\<^sub>3)
  then show ?case by (cases t\<^sub>1) auto
qed blast+

lemma trace_preE [elim]:
  assumes "P\<^sub>1 \<mapsto>x#t\<^sub>2\<^sup>* P\<^sub>3"
  obtains P\<^sub>2 where "P\<^sub>1 \<mapsto>([x])\<^sup>* P\<^sub>2" "P\<^sub>2 \<mapsto>t\<^sub>2\<^sup>* P\<^sub>3"
  using assms by (metis append_Cons append_Nil trace_concatE)

inductive trace_mem
  where 
    [intro]: "trace_mem m [] m" | 
    [intro]: "(m'', m) \<in> eval \<alpha> \<Longrightarrow> trace_mem m t m' \<Longrightarrow> trace_mem m'' (\<alpha>#t) m'"

definition ev :: "'a com \<Rightarrow> ('b \<Rightarrow> 'c) \<Rightarrow> 'a list \<Rightarrow> 'a com  \<Rightarrow>  ('b \<Rightarrow> 'c) \<Rightarrow> bool"
  ("\<langle>_,_\<rangle> \<rightarrow>_\<^sup>* \<langle>_,_\<rangle>" [50,40,40] 70)
  where "\<langle>c,m\<rangle> \<rightarrow>t\<^sup>* \<langle>c',m'\<rangle> \<equiv> trace_mem m t m' \<and> c \<mapsto>t\<^sup>* c'"

lemma trace_mem_det:
  assumes "trace_mem m t m'" "trace_mem m t m''"
  shows "m' = m''"
  using assms
proof (induct arbitrary: m'')
  case (1 m)
  then show ?case by (auto elim: trace_mem.cases)
next
  case (2 m''' m  \<alpha> t m')
  hence a: "(m''', m) \<in> eval \<alpha>" "trace_mem m t m'" "\<forall>m''. trace_mem m t m'' \<longrightarrow> m' = m''"
    by blast+    
  show ?case using 2(4)
  proof (cases rule: trace_mem.cases)
    case (2 m'''')
    show ?thesis using beh_det[OF 2(1) a(1)] a(3) 2(2) by blast
  qed
qed

lemma ev_det:
  assumes "\<langle>c,m\<rangle> \<rightarrow>t\<^sup>* \<langle>c',m'\<rangle>"
  assumes "\<langle>c,m\<rangle> \<rightarrow>t\<^sup>* \<langle>c'',m''\<rangle>"
  shows "m'' = m'"
  using assms trace_mem_det unfolding ev_def
  by auto

end

end