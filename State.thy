theory State
  imports Predicates Stability
begin

section \<open>Specification Record\<close>

record ('r,'v,'s) spec =
  rely :: "('r,'v,'s) prel"
  guar :: "('r,'v,'s) prel"
  sec :: "('r \<Rightarrow> 'v) \<Rightarrow> 'r \<Rightarrow> 's"
  vis :: "'s"

section \<open>State Record\<close>

text \<open>The full state use for the analysis\<close>
record ('a,'r,'v,'s) state =
  mem :: "('r,'v,'s) pred"
  wrs :: "'a \<Rightarrow> 'r set"
  lds :: "'a \<Rightarrow> 'r set"
  ind :: "'a \<Rightarrow> 'r set"
  use :: "'a \<Rightarrow> 'r set"

subsection \<open>Operations of Records\<close>

abbreviation st :: "('b,'c,'d) spec \<Rightarrow> ('a,'b,'c,'d) state \<Rightarrow> bool"
  where "st \<L> P \<equiv> stable (rely \<L>) (mem P)"

definition state_ord (infix "\<preceq>" 60)
  where "state_ord P Q \<equiv> mem P \<subseteq> mem Q \<and> 
                     (\<forall>\<alpha>. wrs Q \<alpha> \<subseteq> wrs P \<alpha> \<and> 
                          lds Q \<alpha> \<subseteq> lds P \<alpha> \<and> 
                          ind Q \<alpha> \<subseteq> ind P \<alpha> \<and> 
                          use Q \<alpha> \<subseteq> use P \<alpha>)"

section \<open>Order Properties\<close>

subsection \<open>Transitive & Reflexive\<close>

lemma state_ord_refl [intro]:
  "P \<preceq> P"
  by (auto simp: state_ord_def)

lemma state_ord_trans [intro,trans]:
  "P \<preceq> Q \<Longrightarrow> Q \<preceq> M \<Longrightarrow> P \<preceq> M"
  by (auto simp: state_ord_def) blast+

subsection \<open>Field Ordering\<close>

lemma wrs_ord [intro]:
  assumes "P \<preceq> Q"
  shows "x \<in> wrs Q \<alpha> \<Longrightarrow> x \<in> wrs P \<alpha>"
  using assms unfolding state_ord_def by auto

lemma lds_ord [intro]:
  assumes "P \<preceq> Q"
  shows "x \<in> lds Q \<alpha> \<Longrightarrow> x \<in> lds P \<alpha>"
  using assms unfolding state_ord_def by auto

lemma use_ord [intro]:
  assumes "P \<preceq> Q"
  shows "x \<in> use Q \<alpha> \<Longrightarrow> x \<in> use P \<alpha>"
  using assms unfolding state_ord_def by auto

subsection \<open>Weakest State\<close>

definition TOP :: "('a,'b,'c,'d) state"
  where "TOP = \<lparr>mem = UNIV, wrs = \<lambda>_. {}, lds = \<lambda>_. {}, ind = \<lambda>_. {}, use = \<lambda>_. {}\<rparr>"

lemma TOP [intro]:
  "P \<preceq> TOP"
  unfolding TOP_def state_ord_def by auto

subsection \<open>Meet operator\<close>

definition both :: "('a,'b,'c,'d) state \<Rightarrow> ('a,'b,'c,'d) state \<Rightarrow> ('a,'b,'c,'d) state"
  where "both a b = \<lparr> mem = mem a \<inter> mem b, 
                      wrs = \<lambda>c. wrs a c \<union> wrs b c, 
                      lds = \<lambda>c. lds a c \<union> lds b c, 
                      ind = \<lambda>c. ind a c \<union> ind b c, 
                      use = \<lambda>c. use a c \<union> use b c \<rparr>"

lemma imp_both [intro]:
  assumes "P \<preceq> P'" "P \<preceq> P''"
  shows "P \<preceq> both P' P''"
  using assms unfolding both_def state_ord_def by auto

lemma imp_bothl [intro]:
  "both P' P'' \<preceq> P'"
  unfolding both_def state_ord_def by auto

lemma imp_bothr [intro]:
  "both P' P'' \<preceq> P''"
  unfolding both_def state_ord_def by auto

lemma stable_both [intro]:
  assumes "st \<L> P" "st \<L> Q"
  shows "st \<L> (both P Q)"
  using assms unfolding both_def by auto

lemma fn_eqI:
  assumes "\<forall>x. f x = g x"
  shows "f = g"
  using assms by auto

end