theory Instructions
  imports State Reordering Security
begin

abbreviation partial
  where "partial b \<equiv> fst ` b \<noteq> UNIV"

definition wfs
  where "wfs \<L> \<equiv> transitive (rely \<L>)"

locale instructions = reordering +
  fixes eval :: "'a \<Rightarrow> ('b \<Rightarrow> 'c) rel"
  fixes flow :: "'a \<Rightarrow> 'd::bounded_lattice \<Rightarrow> ('b \<Rightarrow> 'd) rel"
  fixes rd :: "'a \<Rightarrow> 'b set"
  fixes wr :: "'a \<Rightarrow> 'b set"
  assumes flow_sound: "(m\<^sub>1,m\<^sub>1') \<in> eval \<alpha> \<Longrightarrow> (m\<^sub>2,m\<^sub>2') \<in> eval \<alpha> \<Longrightarrow> (\<gamma>,\<gamma>') \<in> flow \<alpha> (vis \<L>) \<Longrightarrow> \<L> \<turnstile> m\<^sub>1 =\<^bsub>\<gamma>\<^esub> m\<^sub>2 \<Longrightarrow> \<L> \<turnstile> m\<^sub>1' =\<^bsub>\<gamma>'\<^esub> m\<^sub>2'"
  assumes flow_def: "\<not> partial (flow \<alpha> v)"

  assumes wre: "\<And>m m'. (m,m') \<in> eval \<alpha> \<Longrightarrow> m \<approx>-wr \<alpha>\<approx> m'"
  assumes wrf: "\<And>\<gamma> \<gamma>' \<alpha> s. (\<gamma>,\<gamma>') \<in> flow \<alpha> s \<Longrightarrow> \<gamma> \<approx>-wr \<alpha>\<approx> \<gamma>'"
 
  assumes rde: "\<And>m m' n. m \<approx>rd \<alpha>\<approx>m' \<Longrightarrow> (m,n) \<in> eval \<alpha> \<Longrightarrow> (\<exists>n'. (m',n') \<in> eval \<alpha> \<and> n \<approx>wr \<alpha>\<approx> n')"
  assumes rdf: "\<And>\<gamma> \<gamma>' \<gamma>'' \<alpha> s. \<gamma> \<approx>rd \<alpha>\<approx>\<gamma>'' \<Longrightarrow> 
      (\<gamma>,\<gamma>') \<in> flow \<alpha> s \<Longrightarrow> (\<exists>\<gamma>'''. (\<gamma>'',\<gamma>''') \<in> flow \<alpha> s \<and> \<gamma>' \<approx>wr \<alpha>\<approx> \<gamma>''')"

  assumes refacts: "\<And>\<alpha>' \<beta> \<alpha>. \<alpha>' < \<beta> <\<^sub>a \<alpha> \<Longrightarrow> rd \<alpha>' \<subseteq> - rd \<beta> \<and> wr \<alpha>' \<subseteq> - rd \<beta> \<and> wr \<alpha> \<subseteq> - wr \<beta> \<and> wr \<alpha>' = wr \<alpha> \<and> rd \<alpha>' = rd \<alpha> - wr \<beta>"
  assumes fwdable: "\<forall>e' \<alpha> e. e' < \<alpha> <\<^sub>a e \<longrightarrow> e \<noteq> e' \<longrightarrow> wr e = {} \<and> \<not> partial (eval e')"
  assumes specable: "\<forall>e' \<alpha> e. e' < \<alpha> <\<^sub>a e \<longrightarrow> partial (eval \<alpha>) \<longrightarrow> wr e = {} \<and> \<not> partial (eval e')"
  assumes beh_ree: "\<And>\<alpha>' \<beta> \<alpha> m\<^sub>1 m\<^sub>2 m\<^sub>3. \<alpha>' < \<beta> <\<^sub>a \<alpha> \<Longrightarrow> (m\<^sub>1,m\<^sub>2) \<in> eval \<alpha>' \<Longrightarrow> (m\<^sub>2,m\<^sub>3) \<in> eval \<beta> \<Longrightarrow> 
                                                 \<exists>m. (m\<^sub>1,m) \<in> eval \<beta> \<and> (m,m\<^sub>3) \<in> eval \<alpha>"
  assumes beh_ref: "\<And>\<alpha>' \<beta> \<alpha> m\<^sub>1 m\<^sub>2 m\<^sub>3 s. \<alpha>' < \<beta> <\<^sub>a \<alpha> \<Longrightarrow> (m\<^sub>1,m\<^sub>2) \<in> flow \<alpha>' s \<Longrightarrow> (m\<^sub>2,m\<^sub>3) \<in> flow \<beta> s \<Longrightarrow> 
                                                 \<exists>m. (m\<^sub>1,m) \<in> flow \<beta> s \<and> (m,m\<^sub>3) \<in> flow \<alpha> s"

context instructions
begin

abbreviation vars
  where "vars \<alpha> \<equiv> wr \<alpha> \<union> rd \<alpha>"

abbreviation scope
  where "scope P \<alpha> \<equiv> (wrs P \<alpha> \<inter> lds P \<alpha>) \<union> wr \<alpha>"

definition beh
  where "beh \<L> \<alpha> =     
    {(m,m').  (\<A> m,\<A> m') \<in> eval \<alpha> \<and> (\<Gamma> m, \<Gamma> m') \<in> flow \<alpha> (vis \<L>)}"

lemma not_partial:
  assumes "\<not> partial (eval \<beta>)"
  obtains m' where "(m,m') \<in> beh \<L> \<beta>"
proof -
  obtain m' where m: "(\<A> m,m') \<in> eval \<beta>" using assms apply auto
    by (metis (no_types, lifting) eq_fst_iff fst_conv imageE rangeI range_fst)
  obtain \<gamma>' where "(\<Gamma> m,\<gamma>') \<in> flow \<beta> (vis \<L>)" using flow_def apply auto
    by (metis (no_types, lifting) eq_fst_iff fst_conv imageE rangeI range_fst)
  hence "(m,m' \<Otimes> \<gamma>') \<in> beh \<L> \<beta>" using m unfolding beh_def by auto
  thus ?thesis using that by auto
qed

lemma merge_eq:
  assumes "\<A> m \<approx>V\<approx> \<A> m'" "\<Gamma> m \<approx>V\<approx> \<Gamma> m'"
  shows "m \<approx>V\<approx> m'"
  using assms apply (auto simp: partial_equiv_def)
  by (case_tac "m x"; case_tac "m' x") auto

lemma rd:
  assumes "m \<approx>rd \<beta>\<approx>m'" "(m,n) \<in> beh \<L> \<beta>"
  obtains n' where "(m',n') \<in> beh \<L> \<beta>" "n \<approx>wr \<beta>\<approx> n'"
proof -
  have a: "\<A> m \<approx>rd \<beta>\<approx> \<A> m'" using assms(1) by (auto simp: partial_equiv_def)
  have b: "(\<A> m, \<A> n) \<in> eval \<beta>" using assms(2) by (auto simp: beh_def)
  obtain n' where n: "(\<A> m',n') \<in> eval \<beta>" "\<A> n \<approx>wr \<beta>\<approx> n'" using rde[OF a b] by blast

  have c: "\<Gamma> m \<approx>rd \<beta>\<approx> \<Gamma> m'" using assms(1) by (auto simp: partial_equiv_def)
  have d: "(\<Gamma> m, \<Gamma> n) \<in> flow \<beta> (vis \<L>) " using assms(2) by (auto simp: beh_def)
  obtain \<gamma>' where g: "(\<Gamma> m',\<gamma>') \<in> flow \<beta> (vis \<L>) " "\<Gamma> n \<approx>wr \<beta>\<approx> \<gamma>'" 
    using rdf[OF c d] by blast

  have "(m',n' \<Otimes> \<gamma>') \<in> beh \<L> \<beta>" using g n by (auto simp: beh_def)
  moreover have "n \<approx>wr \<beta>\<approx> (n' \<Otimes> \<gamma>')" using  g n apply (auto simp: partial_equiv_def)
    apply (case_tac "n x"; case_tac "(n' \<Otimes> \<gamma>') x")
    by auto
  ultimately show ?thesis using that by blast
qed

lemma wr:
  assumes "(m, m') \<in> beh \<L> \<alpha>"
  shows "m \<approx>- wr \<alpha>\<approx> m'"
proof -
  have e: "(\<A> m, \<A> m') \<in> eval \<alpha>" "(\<Gamma> m, \<Gamma> m') \<in> flow \<alpha> (vis \<L>)"
    using assms unfolding beh_def by auto
  show ?thesis using merge_eq[OF wre[OF e(1)] wrf[OF e(2)]] .
qed 

lemma beh_re:
  assumes "\<alpha>' < \<beta> <\<^sub>a \<alpha>" assumes "(m\<^sub>1,m\<^sub>2) \<in> beh \<L> \<alpha>'" assumes "(m\<^sub>2,m\<^sub>3) \<in> beh \<L> \<beta>"  
  obtains m where "(m\<^sub>1,m) \<in> beh \<L> \<beta>" "(m,m\<^sub>3) \<in> beh \<L> \<alpha>"
proof -
  obtain m' where c: "(\<A> m\<^sub>1,m') \<in> eval \<beta>" "(m',\<A> m\<^sub>3) \<in> eval \<alpha>"
    using beh_ree  assms by (auto simp: beh_def) blast
  obtain \<gamma>' where d: "(\<Gamma> m\<^sub>1,\<gamma>') \<in> flow \<beta> (vis \<L>)" "(\<gamma>',\<Gamma> m\<^sub>3) \<in> flow \<alpha> (vis \<L>)"
    using beh_ref  assms by (auto simp: beh_def) blast
  hence "(m\<^sub>1,m' \<Otimes> \<gamma>') \<in> beh \<L> \<beta>" "(m' \<Otimes> \<gamma>',m\<^sub>3) \<in> beh \<L> \<alpha>" 
    using c  by (auto simp: beh_def)
  thus ?thesis using that by blast
qed

subsection \<open>Context Updates\<close>

definition sp :: "('b,'c,'d) pred \<Rightarrow> ('b,'c,'d) spec \<Rightarrow> 'a \<Rightarrow> ('b,'c,'d) pred" 
  where "sp p \<L> \<alpha> \<equiv> {m. \<exists>m'. m' \<in> p \<and> (m',m) \<in> beh \<L> \<alpha>}"

lemma sp_mono [intro]:
  assumes "p \<subseteq> q"
  shows "sp p \<L> \<alpha> \<subseteq> sp q \<L> \<alpha>"
  using assms unfolding sp_def by auto

definition indirect
  where "indirect R V \<equiv> {x. \<exists>c r. (x,c,r) \<in> R \<and> \<not>(\<forall>m m'. m \<approx>-V\<approx> m' \<longrightarrow> (m \<in> c) = (m' \<in> c))}"

definition sp\<^sub>E :: "('b,'c,'d) pred \<Rightarrow> ('b,'c,'d) spec \<Rightarrow> 'a \<Rightarrow> ('b,'c,'d) pred" 
  where "sp\<^sub>E p \<L> \<alpha> \<equiv> {m\<^sub>3. \<exists>m\<^sub>1 m\<^sub>2. m\<^sub>1 \<in> p \<and> (m\<^sub>1,m\<^sub>2) \<in> beh \<L> \<alpha> \<and> (m\<^sub>2,m\<^sub>3) \<in> prel (rely \<L>)}"

lemma sp\<^sub>E_mono:
  assumes "p \<subseteq> q"
  shows "sp\<^sub>E p \<L> \<alpha> \<subseteq> sp\<^sub>E q \<L> \<alpha>"
  using assms unfolding sp\<^sub>E_def by auto 

subsection \<open>State Manipulation\<close>

lemma [mono]:
  "(\<And>x. P x \<longrightarrow> Q x) \<Longrightarrow> m \<approx>- {xa. P xa}\<approx> m' \<longrightarrow> m \<approx>- {xa. Q xa}\<approx> m'"
  by (auto simp: partial_equiv_def)

inductive_set rind :: "'b set \<Rightarrow> ('b,'c,'d) spec \<Rightarrow> 'b set"
  for V :: "'b set" 
  and \<L> :: "('b,'c,'d) spec"
  where
    "x \<in> V \<Longrightarrow> x \<in> rind V \<L>" |
    "m \<approx>-rind V \<L>\<approx> m' \<Longrightarrow> m \<in> c \<Longrightarrow> m' \<notin> c \<Longrightarrow> (x,c,r) \<in> rely \<L> \<Longrightarrow> x \<in> rind V \<L>"

abbreviation sphere
  where "sphere \<L> i \<alpha> \<equiv> vars \<alpha> \<union> i \<union> rind (vars \<alpha> \<union> i) \<L>"

definition upd :: "('a,'b,'c,'d) state \<Rightarrow> 'a \<Rightarrow> 'b set \<Rightarrow> ('b,'c,'d) spec \<Rightarrow> ('a,'b,'c,'d) state" ("_+[_,_]\<^sub>_" [100,0,0,100] 100) 
  where "upd P \<alpha> i \<L> \<equiv> \<lparr> mem = sp\<^sub>E (mem P) \<L> \<alpha>, 
                     wrs = (\<lambda>\<beta>. case reorder \<alpha> \<beta> of Some \<beta>' \<Rightarrow> wrs P \<beta>' \<inter> wrs P \<beta> - wr \<alpha> | None \<Rightarrow> wrs P \<beta> \<union> wrs P \<alpha>),
                     lds = (\<lambda>\<beta>. case reorder \<alpha> \<beta> of Some \<beta>' \<Rightarrow> lds P \<beta>' \<inter> lds P \<beta> - rd \<alpha> | None \<Rightarrow> lds P \<beta> \<union> lds P \<alpha>),
                     ind = (\<lambda>\<beta>. case reorder \<alpha> \<beta> of Some \<beta>' \<Rightarrow> ind P \<beta>' \<inter> ind P \<beta> - i | None \<Rightarrow> ind P \<beta> \<union> ind P \<alpha>),
                     use = (\<lambda>\<beta>. case reorder \<alpha> \<beta> of Some \<beta>' \<Rightarrow> use P \<beta>' \<inter> use P \<beta> - sphere \<L> i \<alpha> | None \<Rightarrow> use P \<beta> \<union> use P \<alpha>) \<rparr>"

definition ord :: "('a,'b,'c,'d) state \<Rightarrow> 'a \<Rightarrow> 'b set \<Rightarrow> ('b,'c,'d) spec \<Rightarrow> ('a,'b,'c,'d) state" ("_\<oplus>[_,_]\<^sub>_" [100,0,0,100] 100) 
  where "ord P \<alpha> i \<L> \<equiv> \<lparr> mem = sp\<^sub>E (mem P) \<L> \<alpha>, 
                     wrs = (\<lambda>\<beta>. wrs P \<beta> \<union> wrs P \<alpha> \<union> wr \<alpha>),
                     lds = (\<lambda>\<beta>. lds P \<beta> \<union> lds P \<alpha> \<union> wr \<alpha>),
                     ind = (\<lambda>\<beta>. ind P \<beta> \<union> ind P \<alpha>),
                     use = (\<lambda>\<beta>. use P \<beta> \<union> use P \<alpha>) \<rparr>"

definition hide :: "('a,'b,'c,'d) state \<Rightarrow> 'a \<Rightarrow> 'b set \<Rightarrow> ('b,'c,'d) spec \<Rightarrow> ('a,'b,'c,'d) state" ("_-[_,_]\<^sub>_" [100,0,0,100] 100) 
  where "hide P \<alpha> i \<L> \<equiv> \<lparr> mem = mem P, 
                     wrs = (\<lambda>\<beta>. wrs P \<beta> - wr \<alpha>),
                     lds = (\<lambda>\<beta>. lds P \<beta> - rd \<alpha>),
                     ind = (\<lambda>\<beta>. ind P \<beta> - i),
                     use = (\<lambda>\<beta>. use P \<beta> - sphere \<L> i \<alpha>) \<rparr>"

lemma [simp]:
  "mem (P-[\<beta>,i]\<^sub>\<L>) = mem P"
  "use (P-[\<beta>,i]\<^sub>\<L>) \<alpha> = use P \<alpha> - sphere \<L> i \<beta>"
  by (auto simp: hide_def)

lemma [intro]:
  "P \<preceq> P-[\<beta>,i]\<^sub>\<L>"
  by (auto simp: state_ord_def hide_def)

lemma upd_mono [intro]:
  assumes "P \<preceq> Q"
  shows "P+[a,i]\<^sub>\<L> \<preceq> Q+[a,i]\<^sub>\<L>"
  unfolding state_ord_def upd_def using assms 
  by (clarsimp split: option.splits, intro conjI sp\<^sub>E_mono allI) (auto simp: state_ord_def)

lemma ord_mono [intro]:
  assumes "P \<preceq> Q"
  shows "P\<oplus>[a,i]\<^sub>\<L> \<preceq> Q\<oplus>[a,i]\<^sub>\<L>"
  unfolding state_ord_def ord_def using assms 
  by (clarsimp split: option.splits, intro conjI sp\<^sub>E_mono allI) (auto simp: state_ord_def)

lemma ord_upd [intro]:
  "P\<oplus>[\<beta>,i]\<^sub>\<L> \<preceq> P+[\<beta>,i]\<^sub>\<L>"
  unfolding upd_def ord_def state_ord_def
  by (auto simp: split: option.splits)

lemma reorder_upd_wrs [simp]:
  assumes "\<alpha>' < \<beta> <\<^sub>a \<alpha>"
  shows "wrs (P+[\<beta>,i]\<^sub>\<L>) \<alpha> = wrs P \<alpha>' \<inter> wrs P \<alpha> - wr \<beta>"
  using assms unfolding upd_def by auto

lemma reorder_upd_lds [simp]:
  assumes "\<alpha>' < \<beta> <\<^sub>a \<alpha>"
  shows "lds (P+[\<beta>,i]\<^sub>\<L>) \<alpha> = lds P \<alpha>' \<inter> lds P \<alpha>  - rd \<beta>"
  using assms unfolding upd_def by auto

lemma reorder_upd_use [simp]:
  assumes "\<alpha>' < \<beta> <\<^sub>a \<alpha>"
  shows "use (P+[\<beta>,i]\<^sub>\<L>) \<alpha> = use P \<alpha>' \<inter> use P \<alpha> - sphere \<L> i \<beta>"
  using assms unfolding upd_def by auto

text \<open>The predicate produced by the ordered update operation must be stable\<close>
lemma stable_ord [intro]:
  "transitive (rely \<L>) \<Longrightarrow> st \<L> (P\<oplus>[\<alpha>,i]\<^sub>\<L>)"
proof (clarsimp simp: stable_def ord_def sp\<^sub>E_def)
  fix m\<^sub>1 m\<^sub>2 m\<^sub>3 m\<^sub>4 
  assume "transitive (rely \<L>)" "rstep (rely \<L>) m\<^sub>2 m\<^sub>3" "rstep (rely \<L>) m\<^sub>3 m\<^sub>4"
  hence "rstep (rely \<L>) m\<^sub>2 m\<^sub>4" by (rule rstep_transitive)
  moreover assume "m\<^sub>1 \<in> mem P" "(m\<^sub>1, m\<^sub>2) \<in> beh \<L> \<alpha>" 
  ultimately show "\<exists>m\<^sub>1. m\<^sub>1 \<in> mem P \<and> (\<exists>m\<^sub>2. (m\<^sub>1, m\<^sub>2) \<in> beh \<L> \<alpha> \<and> rstep (rely \<L>) m\<^sub>2 m\<^sub>4)" by auto
qed

text \<open>The predicate produced by the unordered update operation must be stable\<close>
lemma stable_upd [intro]:
  "transitive (rely \<L>) \<Longrightarrow> st \<L> (P+[\<alpha>,i]\<^sub>\<L>)"
proof (clarsimp simp: stable_def upd_def sp\<^sub>E_def)
  fix m\<^sub>1 m\<^sub>2 m\<^sub>3 m\<^sub>4 
  assume "transitive (rely \<L>)" "rstep (rely \<L>) m\<^sub>2 m\<^sub>3" "rstep (rely \<L>) m\<^sub>3 m\<^sub>4"
  hence "rstep (rely \<L>) m\<^sub>2 m\<^sub>4" by (rule rstep_transitive)
  moreover assume "m\<^sub>1 \<in> mem P" "(m\<^sub>1, m\<^sub>2) \<in> beh \<L> \<alpha>" 
  ultimately show "\<exists>m\<^sub>1. m\<^sub>1 \<in> mem P \<and> (\<exists>m\<^sub>2. (m\<^sub>1, m\<^sub>2) \<in> beh \<L> \<alpha> \<and> rstep (rely \<L>) m\<^sub>2 m\<^sub>4)" by auto
qed

subsection \<open>Checks\<close>

abbreviation pred :: "('a,'b,'c,'d) state \<Rightarrow> 'b set \<Rightarrow> 'a \<Rightarrow> ('b,'c,'d) pred" 
  where "pred P i \<alpha> \<equiv> restrict (mem P) (vars \<alpha> \<union> i)"

definition stronger ::  "('b,'c,'d) spec \<Rightarrow> ('a,'b,'c,'d) state \<Rightarrow> 'b set \<Rightarrow> 'a \<Rightarrow> 'b set" 
  where "stronger \<L> P i \<alpha> \<equiv> {x. \<exists>c r. (x,c,r) \<in> guar \<L> \<and> \<not> sp (pred P i \<alpha> \<inter> -c) \<L> \<alpha> \<subseteq> -c}"

definition weaker ::  "('b,'c,'d) spec \<Rightarrow> ('a,'b,'c,'d) state \<Rightarrow> 'b set \<Rightarrow> 'a \<Rightarrow> 'b set" 
  where "weaker \<L> P i \<alpha> \<equiv> {x. \<exists>c r. (x,c,r) \<in> rely \<L> \<and> \<not> (sp (pred P i \<alpha> \<inter> c) \<L> \<alpha> \<subseteq> c)}"

lemma pred_ord:
  assumes "P \<preceq> Q"
  shows "pred P i \<alpha> \<subseteq> pred Q i \<alpha>"
  using assms by (auto simp: state_ord_def restrict_def partial_equiv_def)

lemma stronger_ord [intro]:
  assumes "P \<preceq> Q"
  shows "x \<in> stronger \<L> P i \<alpha> \<Longrightarrow> x \<in> stronger \<L> Q i \<alpha>"
  using pred_ord[OF assms, of \<alpha> i]
  by (auto simp: sp_def stronger_def)

lemma weaker_ord [intro]:
  assumes "P \<preceq> Q"
  shows "x \<in> weaker \<L> P i \<alpha> \<Longrightarrow> x \<in> weaker \<L> Q i \<alpha>"
  using pred_ord[OF assms, of \<alpha> i]
  by (auto simp: sp_def weaker_def)

lemma reorder_upd_pred:
  assumes "\<alpha> < \<beta> <\<^sub>a \<alpha>" "\<not> partial (eval \<beta>)"  
  assumes "j \<subseteq> scope (P+[\<beta>,i]\<^sub>\<L>) \<alpha>"
  shows "pred (P-[\<beta>,i]\<^sub>\<L>) j \<alpha> \<subseteq> pred (P+[\<beta>,i]\<^sub>\<L>) j \<alpha>" (is "pred ?N j \<alpha> \<subseteq> pred ?M j \<alpha>")
proof -
  have "mem ?M = sp\<^sub>E (mem P) \<L> \<beta>"
    using assms unfolding state_ord_def upd_def by auto
  thus ?thesis using assms
  proof (clarsimp simp: restrict_def upd_def)
    fix x m' assume a: "m' \<in> mem P" "x \<approx>vars \<alpha> \<union> j\<approx> m'"
    have s: "vars \<alpha> \<union> j \<subseteq> - wr \<beta>" using assms(1,3) refacts[OF assms(1)] by auto
    obtain m'' where m: "(m',m'') \<in> beh \<L> \<beta>" using not_partial assms(2) by blast
    hence "m'' \<in> sp\<^sub>E (mem P) \<L> \<beta>" using a by (auto simp: sp\<^sub>E_def)
    moreover have "x \<approx>vars \<alpha> \<union> j\<approx> m''"
    proof -
      have "m' \<approx>- wr \<beta>\<approx> m''" using m wr by blast
      hence "m' \<approx>vars \<alpha> \<union> j\<approx> m''" apply (rule partial_equiv_sub) using s .
      moreover have "x \<approx>vars \<alpha> \<union> j\<approx> m'"
        using a(2) apply (rule partial_equiv_sub) using assms(1) by blast
      ultimately show ?thesis using partial_equiv_trans by blast
    qed
    ultimately show "\<exists>m'\<in>sp\<^sub>E (mem P) \<L> \<beta>. x \<approx>vars \<alpha> \<union> j\<approx> m'" by blast
  qed
qed

end

end