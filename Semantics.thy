theory Semantics
  imports Instructions Reordering State
begin

chapter \<open>Small Step Semantics\<close>

locale semantics = instructions 

context semantics
begin

section \<open>Program Transition Definitions\<close>

text \<open>
Semantics that collects reordering effects.
Given c \<mapsto>[r,\<alpha>'] c', this corresponds to c \<mapsto>\<alpha><r> c', such that
r should be the program \<alpha>' has to reorder with in c to execute and 
\<alpha> should be \<alpha>' forwarded across r.\<close>
inductive lexecute :: "'a com \<Rightarrow> 'a \<Rightarrow> 'a com \<Rightarrow> 'a \<Rightarrow> 'a com \<Rightarrow> bool"
  ("_ \<mapsto>[_,_,_] _" [71,0,0,0,71] 70)
  where
  act[intro]: "Basic e \<mapsto>[e,Skip,e] Skip" |
  ino[intro]: "c\<^sub>1 \<mapsto>[e',r,e] c\<^sub>1' \<Longrightarrow> c\<^sub>1 ;; c\<^sub>2 \<mapsto>[e',r,e] c\<^sub>1' ;; c\<^sub>2" |
  ooo[intro]: "c\<^sub>1 \<mapsto>[e',r,e] c\<^sub>1' \<Longrightarrow> e'' < c\<^sub>2 <\<^sub>c e' \<Longrightarrow> 
               c\<^sub>2 ;; c\<^sub>1 \<mapsto>[e'',c\<^sub>2 ;; r,e] c\<^sub>2 ;; c\<^sub>1'"
inductive_cases lexecuteE[elim]: "c \<mapsto>[e',p,e] c'"

text \<open>Small step semantics for a silent step\<close>
inductive silent :: "'a com \<Rightarrow> 'a com \<Rightarrow> bool"
  ("_ \<leadsto> _" [71,71] 70)
  where
  seq1[intro]:  "c\<^sub>1 \<leadsto> c\<^sub>1' \<Longrightarrow> c\<^sub>1 ;; c\<^sub>2 \<leadsto> c\<^sub>1' ;; c\<^sub>2" |
  seq2[intro]:  "c\<^sub>2 \<leadsto> c\<^sub>2' \<Longrightarrow> c\<^sub>1 ;; c\<^sub>2 \<leadsto> c\<^sub>1 ;; c\<^sub>2'" |
  seqE1[intro]: "Skip ;; c\<^sub>1 \<leadsto> c\<^sub>1" |
  seqE2[intro]: "c\<^sub>1 ;; Skip \<leadsto> c\<^sub>1" |
  left[intro]:  "c\<^sub>1 \<sqinter> c\<^sub>2 \<leadsto> c\<^sub>1" |
  right[intro]: "c\<^sub>1 \<sqinter> c\<^sub>2 \<leadsto> c\<^sub>2" |
  loop1[intro]: "c* \<leadsto> Skip" |
  loop2[intro]: "c* \<leadsto> c ;; c*"
inductive_cases silentE[elim]: "c\<^sub>1 \<leadsto> c\<^sub>1'"

text \<open>An execution step implies the program has changed\<close>
lemma execute_neq:
  assumes "c \<mapsto>[e,r,e'] c'"
  shows "c \<noteq> c'"
  using assms by (induct) auto

lemma [simp]:
  "c \<mapsto>[e,r,e'] c = False"
  using execute_neq by blast

lemma exec_to_reorder:
  assumes "lexecute c e' r e c'"
  shows "reorder_com e' r e"
  using assms
proof (induct)
  case (act e)
  have "e < Skip <\<^sub>c e" by auto
  then show ?case by meson
next
  case (ino c\<^sub>1 e' r e c\<^sub>1' c\<^sub>2)
  then show ?case by auto
next
  case (ooo c\<^sub>1 e' r e c\<^sub>1' e'' c\<^sub>2)
  then show ?case using reorder_com.simps(3) by meson
qed

end

end