theory General
  imports Trace State
begin

locale rules = trace

context rules
begin

section \<open>Rules\<close>

abbreviation wk_check
  where "wk_check \<L> P i \<alpha> \<equiv> weaker \<L> P i \<alpha> \<subseteq> use P \<alpha>"

abbreviation str_check
  where "str_check \<L> P i \<alpha> \<equiv> stronger \<L> P i \<alpha> \<subseteq> wrs P \<alpha>"

definition progress :: "('b,'c,'d) spec \<Rightarrow> ('a,'b,'c,'d) state \<Rightarrow> 'b set \<Rightarrow> 'a \<Rightarrow> bool"
  where "progress \<L> P i \<alpha> \<equiv> \<forall>m\<^sub>1 m\<^sub>2. m\<^sub>1 =\<^bsub>\<L>,(pred P i \<alpha>)\<^esub> m\<^sub>2 \<longrightarrow> (\<exists>m\<^sub>1'. (m\<^sub>1, m\<^sub>1') \<in> eval \<alpha>) \<longrightarrow> (\<exists>m\<^sub>2'. (m\<^sub>2, m\<^sub>2') \<in> eval \<alpha>)"

definition guarantee :: "('b,'c,'d) spec \<Rightarrow> ('b,'c,'d) mem rel"
  where "guarantee \<L> \<equiv> 
    {(m,m'). \<forall>(x,c,r) \<in> guar \<L>. m \<in> c \<longrightarrow> (m x,m' x) \<in> r\<^sup>=} \<inter>
    {(m,m'). \<forall>x. sec \<L> (\<A> m) x \<ge> \<Gamma> m x \<longrightarrow> sec \<L> (\<A> m') x \<ge> \<Gamma> m' x}"

definition act_po :: "('b,'c,'d) spec \<Rightarrow> ('a,'b,'c,'d) state \<Rightarrow> 'b set \<Rightarrow> 'a \<Rightarrow> bool" 
  where "act_po \<L> P i \<alpha> \<equiv> 
            stronger \<L> P i \<alpha> \<subseteq> wrs P \<alpha> \<and>
            weaker \<L> P i \<alpha> \<subseteq> use P \<alpha> \<and>
            vars \<alpha> \<subseteq> use P \<alpha> \<and>
            vars \<alpha> \<union> i \<subseteq> scope P \<alpha> \<and>
            progress \<L> P i \<alpha> \<and>
            \<lfloor>pred P i \<alpha>\<rfloor> \<inter> beh \<L> \<alpha> \<subseteq> guarantee \<L>"

definition act :: "('b,'c,'d) spec \<Rightarrow> ('a,'b,'c,'d) state \<Rightarrow> 'b set \<Rightarrow> 'a \<Rightarrow> ('a,'b,'c,'d) state \<Rightarrow> bool" 
  where "act \<L> P i \<alpha> Q \<equiv> act_po \<L> P i \<alpha> \<and> P+[\<alpha>,i]\<^sub>\<L> \<preceq> Q"

definition exc :: "('b,'c,'d) spec \<Rightarrow> ('a,'b,'c,'d) state \<Rightarrow> 'b set \<Rightarrow> 'a \<Rightarrow> ('a,'b,'c,'d) state \<Rightarrow> bool" 
  where "exc \<L> P i \<alpha> Q \<equiv> act_po \<L> P i \<alpha> \<and> P\<oplus>[\<alpha>,i]\<^sub>\<L> \<preceq> Q"

text \<open>Establish the rules of the logic, similar to standard Hoare-logic\<close>
inductive rules :: "('b,'c,'d) spec \<Rightarrow> ('a,'b,'c,'d) state \<Rightarrow> 'a com \<Rightarrow> ('a,'b,'c,'d) state \<Rightarrow> bool" 
  ("_ \<turnstile> _ {_} _" [65,0,0,65] 65)
  where
  skip[intro]:   "st \<L> P \<Longrightarrow> \<L> \<turnstile> P { Skip } P" |
  basic[intro]:  "st \<L> P \<Longrightarrow> act \<L> P i \<alpha> Q \<Longrightarrow> \<L> \<turnstile> P { Basic \<alpha> } Q" |
  seq[intro]:    "\<L> \<turnstile> Q { c\<^sub>2 } M \<Longrightarrow> \<L> \<turnstile> P { c\<^sub>1 } Q \<Longrightarrow> \<L> \<turnstile> P { c\<^sub>1 ;; c\<^sub>2 } M" |
  choice[intro]: "\<L> \<turnstile> P { c\<^sub>1 } Q \<Longrightarrow> \<L> \<turnstile> P { c\<^sub>2 } Q \<Longrightarrow> \<L> \<turnstile> P { c\<^sub>1 \<sqinter> c\<^sub>2 } Q" |
  loop[intro]:   "st \<L> P \<Longrightarrow> \<L> \<turnstile> P { c } P \<Longrightarrow> \<L> \<turnstile> P { c* } P" |
  conseq[intro]: "\<L> \<turnstile> P { c } Q \<Longrightarrow> P' \<preceq> P \<Longrightarrow> Q \<preceq> Q' \<Longrightarrow> \<L> \<turnstile> P' { c } Q'"

section \<open>act Properties\<close>

lemma progress_mono:
  assumes "pred Q j \<alpha> \<subseteq> pred P j \<alpha>"
  assumes "progress \<L> P j \<alpha>"
  shows "progress \<L> Q j \<alpha>"
  using assms(2) low_equiv_subI[OF _ assms(1)] unfolding progress_def 
  by blast

lemma act_po_conseq [intro]:
  assumes "act_po \<L> P i \<alpha>" "Q \<preceq> P"
  shows "act_po \<L> Q i \<alpha>"
proof -
  have "\<lfloor>pred Q i \<alpha>\<rfloor> \<inter> beh \<L> \<alpha> \<subseteq> guarantee \<L>"
    using assms pred_ord[OF assms(2), of \<alpha> i] unfolding act_def act_po_def by blast
  moreover have "stronger \<L> Q i \<alpha> \<subseteq> wrs Q \<alpha>"
    using assms unfolding act_def act_po_def by fastforce
  moreover have "weaker \<L> Q i \<alpha> \<subseteq> use Q \<alpha>"
    using assms unfolding act_def act_po_def by fastforce
  moreover have "vars \<alpha> \<subseteq> use Q \<alpha>"
    using assms unfolding act_def act_po_def state_ord_def
    by (meson subset_trans)
  moreover have "vars \<alpha> \<union> i \<subseteq> scope Q \<alpha>"
    using assms unfolding act_def act_po_def state_ord_def
    by (smt (verit, ccfv_threshold) IntD2 IntI UnCI UnE inf.cobounded1 subsetD subsetI)
  moreover have "progress \<L> Q i \<alpha>"
    using progress_mono[OF pred_ord[OF assms(2), of \<alpha> i]] assms
    unfolding act_po_def by auto
  ultimately show ?thesis by (auto simp: act_def act_po_def)
qed

lemma act_conseq:
  assumes "act \<L> P i \<alpha> Q" "P' \<preceq> P" "Q \<preceq> Q'"
  shows "act \<L> P' i \<alpha> Q'"
  using assms unfolding act_def by blast

lemma exc_conseq:
  assumes "exc \<L> P i \<alpha> Q" "P' \<preceq> P" "Q \<preceq> Q'"
  shows "exc \<L> P' i \<alpha> Q'"
  using assms unfolding exc_def by auto

lemma act_to_exec:
  assumes "act \<L> P i e Q"
  shows "exc \<L> P i e Q"
  using assms unfolding act_def exc_def by auto

lemma no_write [simp]:
  assumes "wr \<alpha> = {}" "(m,m') \<in> beh \<L> \<alpha>"
  shows "m = m'"
  using wr[OF assms(2)] assms(1)
  by (auto simp: partial_equiv_def)

lemma stronger_triv [simp]:
  assumes "wfs \<L>"
  assumes "wr \<alpha> = {}"
  shows "stronger \<L> P i \<alpha> = {}"
  unfolding stronger_def sp_def  
  using no_write assms by blast

lemma weaker_triv [simp]:
  assumes "wr \<alpha> = {}"
  shows "weaker \<L> P i \<alpha> = {}"
  unfolding weaker_def sp_def  
  using no_write assms by blast

lemma act_po_triv:
  assumes "wfs \<L>" "wr \<alpha> = {}" "\<not> partial (eval \<alpha>)"
  assumes  "vars \<alpha> \<subseteq> use P \<alpha>" "vars \<alpha> \<union> i \<subseteq> scope P \<alpha>"
  shows "act_po \<L> P i \<alpha>"
  using assms(4,5)
proof (clarsimp simp: act_po_def, intro conjI)
  show "stronger \<L> P i \<alpha> \<subseteq> wrs P \<alpha>" using assms by auto
next
  have test: "\<And>a b. (a, b) \<in> beh \<L> \<alpha> \<Longrightarrow> a = b"
    using wr[of _ _ _ \<alpha>] assms(2) by auto
  have "beh \<L> \<alpha> \<subseteq> guarantee \<L>" 
    unfolding guarantee_def apply (auto)
    apply (subgoal_tac "a = b", auto, rule test, auto)+
    done
  thus "\<lfloor>pred P i \<alpha>\<rfloor> \<inter> beh \<L> \<alpha> \<subseteq> guarantee \<L>" by auto
next
  show "weaker \<L> P i \<alpha> \<subseteq> use P \<alpha>" using assms by auto
next
  show "progress \<L> P i \<alpha>" using assms(3)
    by (auto simp: progress_def) (metis (no_types, lifting) eq_fst_iff fst_conv imageE rangeI range_fst)
qed

section \<open>Expedited Instruction\<close>

lemma expi_weaker:
  assumes "wfs \<L>"
  assumes "\<alpha>' < \<beta> <\<^sub>a \<alpha>"
  assumes "vars \<alpha> \<union> j \<subseteq> scope (P+[\<beta>,i]\<^sub>\<L>) \<alpha>"
  assumes "wk_check \<L> (P+[\<beta>,i]\<^sub>\<L>) j \<alpha>"
  shows "wk_check \<L> (P-[\<beta>,i]\<^sub>\<L>) j \<alpha>'"
  using assms
proof (cases "\<alpha> < \<beta> <\<^sub>a \<alpha> \<and> \<not> partial (eval \<beta>)")
  case True
  have c: "pred (P-[\<beta>,i]\<^sub>\<L>) j \<alpha> \<subseteq> pred (P+[\<beta>,i]\<^sub>\<L>) j \<alpha>" using True reorder_upd_pred assms by blast
  have "\<forall>c. sp (pred (P-[\<beta>,i]\<^sub>\<L>) j \<alpha> \<inter> c) \<L> \<alpha>  \<subseteq> sp (pred (P+[\<beta>,i]\<^sub>\<L>) j \<alpha> \<inter> c) \<L> \<alpha> "
    apply (intro allI) using assms(2) c by (auto simp: hide_def sp_def)
  hence "weaker \<L> (P-[\<beta>,i]\<^sub>\<L>) j \<alpha> \<subseteq> weaker \<L> (P+[\<beta>,i]\<^sub>\<L>) j \<alpha>" unfolding weaker_def by auto
  thus ?thesis using assms True by auto
next
  case False
  hence "wr \<alpha>' = {}" using assms(2) fwdable specable refacts by blast
  thus ?thesis using assms(1) by auto 
qed

lemma expi_stronger:
  assumes "wfs \<L>"
  assumes "\<alpha>' < \<beta> <\<^sub>a \<alpha>"
  assumes "vars \<alpha> \<union> j \<subseteq> scope (P+[\<beta>,i]\<^sub>\<L>) \<alpha>"
  assumes "str_check \<L> (P+[\<beta>,i]\<^sub>\<L>) j \<alpha>"
  shows "str_check \<L> (P-[\<beta>,i]\<^sub>\<L>) j \<alpha>'"
  using assms
proof (cases "\<alpha> < \<beta> <\<^sub>a \<alpha> \<and> \<not> partial (eval \<beta>)")
  case True
  have c: "pred (P-[\<beta>,i]\<^sub>\<L>) j \<alpha> \<subseteq> pred (P+[\<beta>,i]\<^sub>\<L>) j \<alpha>" using True reorder_upd_pred assms by blast
  have "\<forall>c. sp (pred (P-[\<beta>,i]\<^sub>\<L>) j \<alpha> \<inter> c) \<L> \<alpha>  \<subseteq> sp (pred (P+[\<beta>,i]\<^sub>\<L>) j \<alpha> \<inter> c) \<L> \<alpha> "
    apply (intro allI) using assms(2) c by (auto simp: hide_def sp_def)
  hence "stronger \<L> (P-[\<beta>,i]\<^sub>\<L>) j \<alpha> \<subseteq> stronger \<L> (P+[\<beta>,i]\<^sub>\<L>) j \<alpha>" unfolding stronger_def by blast
  thus ?thesis using assms True by (auto simp: hide_def)
next
  case False
  hence "wr \<alpha>' = {}" using assms(2) fwdable specable refacts by blast
  thus ?thesis using assms(1) by auto 
qed

lemma expi_guarantee:
  assumes "vars \<alpha> \<union> j \<subseteq> scope (P+[\<beta>,i]\<^sub>\<L>) \<alpha>"
  assumes "\<alpha> < \<beta> <\<^sub>a \<alpha> \<and> \<not> partial (eval \<beta>)"
  assumes "\<lfloor>pred (P+[\<beta>,i]\<^sub>\<L>) j \<alpha>\<rfloor> \<inter> beh \<L> \<alpha> \<subseteq> guarantee \<L>"
  shows "\<lfloor>pred (P-[\<beta>,i]\<^sub>\<L>) j \<alpha>\<rfloor> \<inter> beh \<L> \<alpha> \<subseteq> guarantee \<L>"
proof -
  have "pred (P-[\<beta>,i]\<^sub>\<L>) j \<alpha> \<subseteq> pred (P+[\<beta>,i]\<^sub>\<L>) j \<alpha>" using assms reorder_upd_pred by auto
  then show ?thesis using assms unfolding pre_def by auto
qed

lemma expi_progress:
  assumes "vars \<alpha> \<union> j \<subseteq> scope (P+[\<beta>,i]\<^sub>\<L>) \<alpha>"
  assumes "\<alpha> < \<beta> <\<^sub>a \<alpha> \<and> \<not> partial (eval \<beta>)"
  assumes "progress \<L> (P+[\<beta>,i]\<^sub>\<L>) j \<alpha>"
  shows "progress \<L> (P-[\<beta>,i]\<^sub>\<L>) j \<alpha>"
proof -
  have "pred (P-[\<beta>,i]\<^sub>\<L>) j \<alpha> \<subseteq> pred (P+[\<beta>,i]\<^sub>\<L>) j \<alpha>" using assms reorder_upd_pred by auto
  then show ?thesis using assms(3) by (rule progress_mono)
qed

lemma expi_act_po:
  assumes "wfs \<L>"
  assumes "\<alpha>' < \<beta> <\<^sub>a \<alpha>"
  assumes "act_po \<L> (P+[\<beta>,i]\<^sub>\<L>) j \<alpha>"
  shows "act_po \<L> (P-[\<beta>,i]\<^sub>\<L>) j \<alpha>'"
proof (cases "\<alpha> < \<beta> <\<^sub>a \<alpha> \<and> \<not> partial (eval \<beta>)")
  case True
  hence [simp]: "\<alpha>' = \<alpha>" using assms(2) by auto
  have j: "vars \<alpha> \<union> j \<subseteq> scope (P+[\<beta>,i]\<^sub>\<L>) \<alpha>" using assms(3) by (auto simp: act_po_def)
  have "stronger \<L> (P-[\<beta>,i]\<^sub>\<L>) j \<alpha>' \<subseteq> wrs (P-[\<beta>,i]\<^sub>\<L>) \<alpha>'" 
    using assms(3) expi_stronger[OF assms(1,2)] unfolding act_po_def by auto
  moreover have "weaker \<L> (P-[\<beta>,i]\<^sub>\<L>) j \<alpha>' \<subseteq> use (P-[\<beta>,i]\<^sub>\<L>) \<alpha>'" 
    using assms(3) expi_weaker[OF assms(1,2)] unfolding act_po_def by auto
  moreover have "\<lfloor>pred (P-[\<beta>,i]\<^sub>\<L>) j \<alpha>'\<rfloor> \<inter> beh \<L> \<alpha>' \<subseteq> guarantee \<L>" 
    using assms(3) expi_guarantee[OF j True] unfolding act_po_def
    using \<open>\<alpha>' = \<alpha>\<close> by presburger
  moreover have "vars \<alpha>' \<subseteq> use (P-[\<beta>,i]\<^sub>\<L>) \<alpha>'"
    using assms(2,3) by (auto simp: act_po_def upd_def hide_def)
  moreover have "vars \<alpha> \<union> j \<subseteq> scope (P-[\<beta>,i]\<^sub>\<L>) \<alpha>'" using assms(2,3) 
    unfolding act_po_def hide_def by auto
  moreover have "progress \<L> (P-[\<beta>,i]\<^sub>\<L>) j \<alpha>'"
    using assms(3) expi_progress[OF j True] unfolding act_po_def by auto
  ultimately show ?thesis unfolding act_po_def by auto
next
  case False
  hence "wr \<alpha>' = {}" "\<not> partial (eval \<alpha>')" using assms(2) fwdable specable refacts by blast+
  moreover have "vars \<alpha>' \<subseteq> use (P-[\<beta>,i]\<^sub>\<L>) \<alpha>'"
    using assms(2,3) refacts[OF assms(2)] by (auto simp: act_po_def upd_def hide_def)
  moreover have "vars \<alpha>' \<union> j \<subseteq> scope (P-[\<beta>,i]\<^sub>\<L>) \<alpha>'" using assms(2,3) refacts[OF assms(2)]
    unfolding act_po_def hide_def by auto
  ultimately show ?thesis using assms(1) act_po_triv by blast
qed

section \<open>Delayed Instruction\<close>

lemma in_sphere:
  assumes "m \<approx>-sphere \<L> i \<beta>\<approx> m'"
  assumes "(m \<in> c) \<noteq> (m' \<in> c)"
  assumes "(x, c, r) \<in> rely \<L>"
  shows "x \<in> sphere \<L> i \<beta>"
proof -
  have "sphere \<L> i \<beta> \<subseteq> rind (vars \<beta> \<union> i) \<L>"
    by (auto intro: rind.intros)
  hence eq: "m \<approx>- rind (vars \<beta> \<union> i) \<L>\<approx> m'" 
    using assms(1) by (auto simp: partial_equiv_def)
  consider "m \<in> c" "m' \<notin> c" | "m' \<in> c" "m \<notin> c"
    using assms(2) by auto
  hence "x \<in> rind (vars \<beta> \<union> i) \<L>"
  proof (cases)
    case 1
    then show ?thesis using eq assms(3) rind.intros(2) by blast
  next
    case 2
    have "m' \<approx>- rind (vars \<beta> \<union> i) \<L>\<approx> m" using eq by (auto simp: partial_equiv_def) 
    then show ?thesis using assms(3) rind.intros(2) 2 by blast
  qed
  thus ?thesis by auto
qed

lemma notin_sphere:
  assumes "(x, c, r) \<in> rely \<L>"
  assumes "m \<approx>-sphere \<L> i \<beta>\<approx> m'"
  assumes "x \<notin> sphere \<L> i \<beta>"
  shows "(m \<in> c) = (m' \<in> c)"
  using in_sphere[OF assms(2) _ assms(1)] assms(3) by auto

lemma env_split:
  assumes "st \<L> P" "m\<^sub>1 \<in> mem P" "(m\<^sub>1,m\<^sub>2) \<in> beh \<L> \<alpha>" "(m\<^sub>2,m\<^sub>3) \<in> prel (rely \<L>)" "wk_check \<L> (P-[\<beta>,i]\<^sub>\<L>) j \<alpha>" 
  assumes indep: "sphere \<L> i \<beta> \<subseteq> - wr \<alpha> \<inter> -rd \<alpha>"
  assumes re: "\<alpha> < \<beta> <\<^sub>a \<alpha>'"
  obtains n\<^sub>1 n\<^sub>2 where "n\<^sub>1 \<in> mem P" "(n\<^sub>1,n\<^sub>2) \<in> beh \<L> \<alpha>" "(n\<^sub>2,m\<^sub>3) \<in> prel (rely \<L>)" "n\<^sub>1 \<approx>sphere \<L> i \<beta>\<approx> m\<^sub>3"
proof -
  let ?V = "sphere \<L> i \<beta>"
  let ?n1 = "\<lambda>v. if v \<in> ?V then m\<^sub>3 v else m\<^sub>1 v"
  let ?n2 = "\<lambda>v. if v \<in> ?V then m\<^sub>3 v else m\<^sub>2 v"

  have [simp]: "(wrs P \<alpha> - wr \<beta>) \<inter> (lds P \<alpha> - rd \<beta>) \<union> wr \<alpha> = scope P \<alpha> - vars \<beta>"
    using refacts[OF re] by (auto simp: hide_def)
  have w: "\<forall>x \<in> ?V. \<forall>c r. (x,c,r) \<in> rely \<L> \<longrightarrow> sp (pred P j \<alpha> \<inter> c) \<L> \<alpha> \<subseteq> c"
    using assms unfolding weaker_def by (auto simp: hide_def)

  have "\<forall>x c r. (x, c, r) \<in> rely \<L> \<longrightarrow> m\<^sub>1 \<in> c \<longrightarrow> x \<in> ?V \<longrightarrow> (m\<^sub>1 x, m\<^sub>3 x) \<in> r\<^sup>*"
  proof (intro allI impI, goal_cases C)
    case (C x c r) 
    have [simp]: "m\<^sub>1 x = m\<^sub>2 x" using indep wr[OF assms(3)] C by (auto simp: partial_equiv_def) 
    have sp: "sp (pred P j \<alpha> \<inter> c) \<L> \<alpha> \<subseteq> c" using C w by auto
    have "{m\<^sub>1} \<subseteq> pred P j \<alpha> \<inter> c" using assms(2) C by (auto simp: restrict_def)
    hence "{m\<^sub>2} \<subseteq> sp (pred P j \<alpha> \<inter> c) \<L> \<alpha>" using assms(3) by (auto simp: sp_def)
    hence "{m\<^sub>2} \<subseteq> c" using sp by auto      
    hence "(m\<^sub>2 x, m\<^sub>3 x) \<in> r\<^sup>*" using assms(4) C(1) by (auto)
    thus ?case by auto
  qed

  hence "(m\<^sub>1,?n1) \<in> prel (rely \<L>)" by auto
  hence p: "?n1 \<in> mem P" using assms(1,2) by (auto simp: stable_def)
  have [simp]: "\<forall>x. x \<notin> wr \<alpha> \<longrightarrow> m\<^sub>2 x = m\<^sub>1 x" 
    using wr[OF assms(3)] by (auto simp: partial_equiv_def)

  have rds: "m\<^sub>1 \<approx>rd \<alpha>\<approx> ?n1" using indep by (auto simp: partial_equiv_def)
  obtain n2 where 2: "(?n1,n2) \<in> beh \<L> \<alpha>" "m\<^sub>2 \<approx>wr \<alpha>\<approx> n2" using rd[OF rds assms(3)] by blast
  have n: "n2 = ?n2" apply (rule fn_eqI) using wr[OF 2(1)] 2(2) indep
    apply (auto simp: partial_equiv_def)
    apply (case_tac "x\<in>wr \<alpha>")
    apply auto[2]
    apply (metis ComplI)
    apply (case_tac "x\<in>wr \<alpha>")
    apply auto
    apply (metis ComplI)
    apply (case_tac "x\<in>wr \<alpha>")
    apply auto
    apply (metis ComplI)
    apply (case_tac "x\<in>wr \<alpha>")
    apply auto
    apply (metis ComplI)
    apply (case_tac "x\<in>wr \<alpha>")
    apply auto
    apply (metis ComplI)
    done

  have "(?n2,m\<^sub>3) \<in> prel (rely \<L>)"
  proof (clarsimp, goal_cases)
    case (1 x c r)
    have "?n2 \<approx>-?V\<approx> m\<^sub>2" unfolding partial_equiv_def by auto
    hence "(?n2 \<in> c) = (m\<^sub>2 \<in> c)" apply (rule notin_sphere[OF 1(1)]) using 1 by auto
    hence "m\<^sub>2 \<in> c" using 1 by auto
    then show ?case using assms(4) 1(1) by auto
  qed
  moreover have "?n1 \<approx>?V\<approx> m\<^sub>3" by (auto simp: partial_equiv_def)
  ultimately show ?thesis using p 2(1) that unfolding n by fastforce
qed

lemma later_env:
  assumes "(m\<^sub>1,m\<^sub>2) \<in> prel (rely \<L>)" "(m\<^sub>2,m\<^sub>3) \<in> beh \<L> \<alpha>"
  assumes "m\<^sub>1 \<approx>sphere \<L> i \<alpha>\<approx> m\<^sub>2"
  obtains m\<^sub>2 where "(m\<^sub>1,m\<^sub>2) \<in> beh \<L> \<alpha>" "(m\<^sub>2,m\<^sub>3) \<in> prel (rely \<L>)" 
proof -
  let ?V = "sphere \<L> i \<alpha>"
  
  have rds: "m\<^sub>2 \<approx>rd \<alpha>\<approx> m\<^sub>1" using assms(3) by (auto simp: partial_equiv_def)
  obtain n where n: "(m\<^sub>1,n) \<in> beh \<L> \<alpha>" "m\<^sub>3 \<approx>wr \<alpha>\<approx> n" using rd[OF rds assms(2)] by blast
  have "n = (\<lambda>x. if x \<in> wr \<alpha> then m\<^sub>3 x else m\<^sub>1 x)"
    apply (intro fn_eqI allI) using n(2) wr[OF n(1)] by (auto simp: partial_equiv_def)
  hence [simp]: "n = (\<lambda>x. if x \<in> ?V then m\<^sub>3 x else m\<^sub>1 x)"
    apply (intro fn_eqI allI) using assms(3) n(2) wr[OF n(1)] wr[OF assms(2)] 
    by (auto simp: partial_equiv_def)
  have "(n,m\<^sub>3) \<in> prel (rely \<L>)"
  proof (auto, goal_cases)
    case (1 x c r)
    have "n \<approx>-?V\<approx> m\<^sub>1" unfolding partial_equiv_def by auto
    hence "(n \<in> c) = (m\<^sub>1 \<in> c)" apply (rule notin_sphere[OF 1(1)]) using 1 by auto
    hence "(m\<^sub>1 x, m\<^sub>2 x) \<in> r\<^sup>*" using assms(1) 1 by auto
    then show ?case using wr[OF assms(2)] 1 by (auto simp: partial_equiv_def)
  qed
  thus ?thesis using that n(1) by auto
qed

lemma delay_guarantee:
  assumes "st \<L> P" "\<alpha>' < \<beta> <\<^sub>a \<alpha>"
  assumes "\<lfloor>pred P i \<beta>\<rfloor> \<inter> beh \<L> \<beta> \<subseteq> guarantee \<L>"
  assumes "wk_check \<L> (P-[\<beta>,i]\<^sub>\<L>) j \<alpha>'" 
  assumes v: "sphere \<L> i \<beta> \<subseteq> - wr \<alpha>' \<inter> - rd \<alpha>'"
  shows "\<lfloor>pred (P\<oplus>[\<alpha>',j]\<^sub>\<L>) i \<beta>\<rfloor> \<inter> beh \<L> \<beta> \<subseteq> guarantee \<L>"
proof (clarsimp simp: pre_def)
  fix m\<^sub>3 m\<^sub>4 
  assume a: "(m\<^sub>3,m\<^sub>4) \<in> beh \<L> \<beta>" "m\<^sub>3 \<in> mem (P\<oplus>[\<alpha>',j]\<^sub>\<L>) \<downharpoonright>\<^sub>E vars \<beta> \<union> i" 

  obtain n\<^sub>3 where n\<^sub>3: "m\<^sub>3 \<approx>vars \<beta> \<union> i\<approx> n\<^sub>3" "n\<^sub>3 \<in> sp\<^sub>E (mem P) \<L> \<alpha>'"
    using a by (auto simp: restrict_def ord_def)
  then obtain n\<^sub>1 n\<^sub>2 where n\<^sub>1: "n\<^sub>1 \<in> mem P" "(n\<^sub>1,n\<^sub>2) \<in> beh \<L> \<alpha>'" "(n\<^sub>2,n\<^sub>3) \<in> prel (rely \<L>)"
    by (auto simp: sp\<^sub>E_def)

  obtain p\<^sub>1 p\<^sub>2 where p\<^sub>1: "p\<^sub>1 \<in> mem P" "(p\<^sub>1,p\<^sub>2) \<in> beh \<L> \<alpha>'" "(p\<^sub>2,n\<^sub>3) \<in> prel (rely \<L>)" 
                        "p\<^sub>1 \<approx>sphere \<L> i \<beta>\<approx> n\<^sub>3"
    using env_split[OF assms(1) n\<^sub>1 assms(4) v assms(2)] by blast

  have "m\<^sub>3 \<approx>vars \<beta> \<union> i\<approx> p\<^sub>1" using n\<^sub>3(1) p\<^sub>1(4) by (auto simp: partial_equiv_def ord_def)
  hence "m\<^sub>3 \<in> mem P \<downharpoonright>\<^sub>E vars \<beta> \<union> i" using p\<^sub>1(1) by (auto simp: restrict_def)

  thus "(m\<^sub>3,m\<^sub>4) \<in> guarantee \<L>" using assms(3) a(1) by (auto simp: pre_def)
qed

lemma delay_progress:
  assumes "st \<L> P" "\<alpha>' < \<beta> <\<^sub>a \<alpha>"
  assumes "progress  \<L> P i \<beta>"
  assumes "wk_check \<L> (P-[\<beta>,i]\<^sub>\<L>) j \<alpha>'" 
  assumes v: "sphere \<L> i \<beta> \<subseteq> - wr \<alpha>' \<inter> - rd \<alpha>'"
  shows "progress \<L> (P\<oplus>[\<alpha>',j]\<^sub>\<L>) i \<beta>"
proof -
  have "pred (P\<oplus>[\<alpha>',j]\<^sub>\<L>) i \<beta> \<subseteq> pred P i \<beta>"
  proof (clarsimp)
    fix m\<^sub>3
    assume a: "m\<^sub>3 \<in> mem (P\<oplus>[\<alpha>',j]\<^sub>\<L>) \<downharpoonright>\<^sub>E vars \<beta> \<union> i" 
    obtain n\<^sub>3 where n\<^sub>3: "m\<^sub>3 \<approx>vars \<beta> \<union> i\<approx> n\<^sub>3" "n\<^sub>3 \<in> sp\<^sub>E (mem P) \<L> \<alpha>'"
      using a by (auto simp: restrict_def ord_def)
    then obtain n\<^sub>1 n\<^sub>2 where n\<^sub>1: "n\<^sub>1 \<in> mem P" "(n\<^sub>1,n\<^sub>2) \<in> beh \<L> \<alpha>'" "(n\<^sub>2,n\<^sub>3) \<in> prel (rely \<L>)"
      by (auto simp: sp\<^sub>E_def)
    obtain p\<^sub>1 p\<^sub>2 where p\<^sub>1: "p\<^sub>1 \<in> mem P" "(p\<^sub>1,p\<^sub>2) \<in> beh \<L> \<alpha>'" "(p\<^sub>2,n\<^sub>3) \<in> prel (rely \<L>)" 
                        "p\<^sub>1 \<approx>sphere \<L> i \<beta>\<approx> n\<^sub>3"
      using env_split[OF assms(1) n\<^sub>1 assms(4) v assms(2)] by blast
    have "m\<^sub>3 \<approx>vars \<beta> \<union> i\<approx> p\<^sub>1" using n\<^sub>3(1) p\<^sub>1(4) by (auto simp: partial_equiv_def ord_def)
    thus "m\<^sub>3 \<in> mem P \<downharpoonright>\<^sub>E vars \<beta> \<union> i" using p\<^sub>1(1) by (auto simp: restrict_def)
  qed
  thus ?thesis using assms(3) low_equiv_subI unfolding progress_def by blast
qed

lemma delay_stronger:
  assumes "st \<L> P" "\<alpha>' < \<beta> <\<^sub>a \<alpha>"
  assumes v: "sphere \<L> i \<beta> \<subseteq> - wr \<alpha>' \<inter> - rd \<alpha>'"
  assumes "wk_check \<L> (P-[\<beta>,i]\<^sub>\<L>) j \<alpha>'" 
  assumes "stronger \<L> P i \<beta> \<subseteq> wrs P \<beta>"
  shows "stronger \<L> (P\<oplus>[\<alpha>',j]\<^sub>\<L>) i \<beta> \<subseteq> wrs (P\<oplus>[\<alpha>',j]\<^sub>\<L>) \<beta>"
proof (clarsimp simp: stronger_def sp_def, goal_cases)
  case (1 x c m\<^sub>4 r m\<^sub>3)
  obtain n\<^sub>3 where n\<^sub>3: "m\<^sub>3 \<approx>vars \<beta> \<union> i\<approx> n\<^sub>3" "n\<^sub>3 \<in> sp\<^sub>E (mem P) \<L> \<alpha>'"
    using 1 by (auto simp: restrict_def ord_def)
  then obtain n\<^sub>1 n\<^sub>2 where n\<^sub>1: "n\<^sub>1 \<in> mem P" "(n\<^sub>1,n\<^sub>2) \<in> beh \<L> \<alpha>'" "(n\<^sub>2,n\<^sub>3) \<in> prel (rely \<L>)"
    by (auto simp: sp\<^sub>E_def)
  obtain p\<^sub>1 p\<^sub>2 where p\<^sub>1: "p\<^sub>1 \<in> mem P" "(p\<^sub>1,p\<^sub>2) \<in> beh \<L> \<alpha>'" "(p\<^sub>2,n\<^sub>3) \<in> prel (rely \<L>)" 
                        "p\<^sub>1 \<approx>sphere \<L> i \<beta>\<approx> n\<^sub>3"
    using env_split[OF assms(1) n\<^sub>1 assms(4) v assms(2)] by blast
  have "sp (pred P i \<beta> \<inter> -c) \<L> \<beta> \<subseteq> -c" using 1(1,3) assms(5)
    by (auto simp: ord_def stronger_def)
  moreover have "m\<^sub>3 \<in> pred P i \<beta>" using p\<^sub>1(1,4) n\<^sub>3(1) 
    by (auto simp: restrict_def partial_equiv_def) (metis Un_iff)
  ultimately show ?case using 1(2,4,5,6) by (auto simp: sp_def)
qed

lemma delay_weaker:
  assumes "st \<L> P" "\<alpha>' < \<beta> <\<^sub>a \<alpha>"
  assumes "weaker \<L> P i \<beta> \<subseteq> use P \<beta>"
  assumes "wk_check \<L> (P-[\<beta>,i]\<^sub>\<L>) j \<alpha>'" 
  assumes v: "sphere \<L> i \<beta> \<subseteq> - wr \<alpha>' \<inter> - rd \<alpha>'"
  shows "weaker \<L> (P\<oplus>[\<alpha>',j]\<^sub>\<L>) i \<beta> \<subseteq> use (P\<oplus>[\<alpha>',j]\<^sub>\<L>) \<beta>"
proof (clarsimp simp: weaker_def sp_def, goal_cases)
  case (1 x c m\<^sub>4 r m\<^sub>3)
  obtain n\<^sub>3 where n\<^sub>3: "m\<^sub>3 \<approx>vars \<beta> \<union> i\<approx> n\<^sub>3" "n\<^sub>3 \<in> sp\<^sub>E (mem P) \<L> \<alpha>'"
    using 1 by (auto simp: restrict_def ord_def)
  then obtain n\<^sub>1 n\<^sub>2 where n\<^sub>1: "n\<^sub>1 \<in> mem P" "(n\<^sub>1,n\<^sub>2) \<in> beh \<L> \<alpha>'" "(n\<^sub>2,n\<^sub>3) \<in> prel (rely \<L>)"
    by (auto simp: sp\<^sub>E_def)
  obtain p\<^sub>1 p\<^sub>2 where p\<^sub>1: "p\<^sub>1 \<in> mem P" "(p\<^sub>1,p\<^sub>2) \<in> beh \<L> \<alpha>'" "(p\<^sub>2,n\<^sub>3) \<in> prel (rely \<L>)" 
                        "p\<^sub>1 \<approx>sphere \<L> i \<beta>\<approx> n\<^sub>3"
    using env_split[OF assms(1) n\<^sub>1 assms(4) v assms(2)] by blast
  have "sp (pred P i \<beta> \<inter> c) \<L> \<beta> \<subseteq> c" using 1(1,2) assms(3) 
    by (auto simp: ord_def weaker_def)
  moreover have "m\<^sub>3 \<in> pred P i \<beta>" using p\<^sub>1(1,4) n\<^sub>3(1) 
    by (auto simp: restrict_def partial_equiv_def) (metis Un_iff)
  ultimately show ?case using 1(4,5) by (auto simp: sp_def)
qed

lemma delay_act_po:
  assumes "st \<L> P"
  assumes "\<alpha>' < \<beta> <\<^sub>a \<alpha>"
  assumes "wk_check \<L> (P-[\<beta>,i]\<^sub>\<L>) j \<alpha>'"
  assumes "sphere \<L> i \<beta> \<subseteq> - wr \<alpha>' \<inter> - rd \<alpha>'"
  assumes a: "act_po \<L> P i \<beta>"
  shows "act_po \<L> (P\<oplus>[\<alpha>',j]\<^sub>\<L>) i \<beta>"
proof (clarsimp simp: act_po_def, intro conjI)
  show "stronger \<L> (P\<oplus>[\<alpha>',j]\<^sub>\<L>) i \<beta> \<subseteq> wrs (P\<oplus>[\<alpha>',j]\<^sub>\<L>) \<beta>"
    using delay_stronger assms by (auto simp: act_po_def)
next
  show "\<lfloor>pred (P\<oplus>[\<alpha>',j]\<^sub>\<L>) i \<beta>\<rfloor> \<inter> beh \<L> \<beta> \<subseteq> guarantee \<L>"
    using delay_guarantee assms by (auto simp: act_po_def)
next
  show "weaker \<L> (P\<oplus>[\<alpha>',j]\<^sub>\<L>) i \<beta> \<subseteq> use (P\<oplus>[\<alpha>',j]\<^sub>\<L>) \<beta>"
    using delay_weaker assms by (auto simp: act_po_def)
next
  show "progress \<L> (P\<oplus>[\<alpha>',j]\<^sub>\<L>) i \<beta>"  
    using delay_progress assms by (auto simp: act_po_def)
qed (insert a, auto simp: act_po_def ord_def)

subsection \<open>Transform Reordering\<close>

lemma reorder_updates:
  assumes "st \<L> P" "\<alpha>' < \<beta> <\<^sub>a \<alpha>" 
  assumes w: "wk_check \<L> (P-[\<beta>,i]\<^sub>\<L>) j \<alpha>'"
  assumes t: "transitive (rely \<L>)"
  assumes v: "sphere \<L> i \<beta> \<subseteq> - wr \<alpha>' \<inter> - rd \<alpha>'"
  shows "(P\<oplus>[\<alpha>',j]\<^sub>\<L>)+[\<beta>,i]\<^sub>\<L> \<preceq> (P+[\<beta>,i]\<^sub>\<L>)\<oplus>[\<alpha>,j]\<^sub>\<L>"
proof (auto simp: state_ord_def, goal_cases)
  case (1 m\<^sub>5)
  then show ?case using assms
  proof (clarsimp simp: upd_def ord_def sp\<^sub>E_def restrict_def)
    fix m\<^sub>1 m\<^sub>2 m\<^sub>3 m\<^sub>4 
    assume p: "m\<^sub>1 \<in> mem P" 
    assume \<alpha>: "(m\<^sub>1,m\<^sub>2) \<in> beh \<L> \<alpha>'" "rstep (rely \<L>) m\<^sub>2 m\<^sub>3" 
    assume \<beta>: "(m\<^sub>3,m\<^sub>4) \<in> beh \<L> \<beta>" "rstep (rely \<L>) m\<^sub>4 m\<^sub>5"
    have e: " (m\<^sub>2, m\<^sub>3) \<in> prel (rely \<L>)" using \<alpha>(2) by auto

    text \<open>Split the central environment step into one that depends on \<alpha>' and one that does not\<close>
    obtain p\<^sub>1 p\<^sub>2 where p\<^sub>1: "p\<^sub>1 \<in> mem P" "(p\<^sub>1,p\<^sub>2) \<in> beh \<L> \<alpha>'" "(p\<^sub>2,m\<^sub>3) \<in> prel (rely \<L>)" 
                          "p\<^sub>1 \<approx>sphere \<L> i \<beta>\<approx> m\<^sub>3"
      using env_split[OF assms(1) p \<alpha>(1) e w v assms(2)] by blast

    hence p: "p\<^sub>2 \<approx>sphere \<L> i \<beta>\<approx> m\<^sub>3"
      using v wr[OF p\<^sub>1(2)] refacts[OF assms(2)] unfolding partial_equiv_def
      by (metis (no_types, lifting) in_mono inf.cobounded1)
    text \<open>Move the second environment step after \<beta>\<close>
    obtain p\<^sub>3 where p\<^sub>3: "(p\<^sub>2,p\<^sub>3) \<in> beh \<L> \<beta>" "(p\<^sub>3,m\<^sub>4) \<in> prel (rely \<L>)"
      using later_env[OF p\<^sub>1(3) \<beta>(1) p] by auto

    text \<open>Reorder the instructions\<close>
    obtain p\<^sub>4 where p\<^sub>4: "(p\<^sub>1,p\<^sub>4) \<in> beh \<L> \<beta>" "(p\<^sub>4,p\<^sub>3) \<in> beh \<L> \<alpha>" using p\<^sub>1 p\<^sub>3(1) assms(2) beh_re 
      by blast

    moreover have "(p\<^sub>4,p\<^sub>4) \<in> prel (rely \<L>)" by (auto)
    moreover have "(p\<^sub>3,m\<^sub>5) \<in> prel (rely \<L>)" using p\<^sub>3(2) \<beta>(2) rstep_transitive[OF t] by auto
    ultimately show "\<exists>m\<^sub>3. (\<exists>m\<^sub>1. 
              m\<^sub>1 \<in> mem P \<and> (\<exists>m\<^sub>2. (m\<^sub>1,m\<^sub>2) \<in> beh \<L> \<beta> \<and> rstep (rely \<L>) m\<^sub>2 m\<^sub>3)) \<and>
             (\<exists>m\<^sub>4. (m\<^sub>3, m\<^sub>4) \<in> beh \<L> \<alpha> \<and> rstep (rely \<L>) m\<^sub>4 m\<^sub>5)"
      using p\<^sub>1(1) by blast
  qed
qed (insert assms refacts[OF assms(2)], auto simp: ord_def upd_def split: option.splits)

section \<open>Elimination Rules\<close>

text \<open>Extract stability from a nil judgement\<close>
lemma skipE [elim!]:
  assumes "\<L> \<turnstile> P {Skip} Q"
  obtains M where "st \<L> M" "P \<preceq> M" "M \<preceq> Q"
  using assms by (induct \<L> P "Skip :: 'a com" Q) auto

lemma seqE [elim]:
  assumes "\<L> \<turnstile> P {c\<^sub>1 ;; c\<^sub>2} Q"
  obtains M where "\<L> \<turnstile> P {c\<^sub>1} M" "\<L> \<turnstile> M {c\<^sub>2} Q"
  using assms by (induct \<L> P "c\<^sub>1 ;; c\<^sub>2" Q arbitrary: c\<^sub>1 c\<^sub>2) blast+

lemma actE [elim]:
  assumes "\<L> \<turnstile> P {Basic \<alpha>} Q"
  assumes "wfs \<L>"
  obtains P' i Q' where "P \<preceq> P'" "st \<L> P'" "act \<L> P' i \<alpha> Q'" "st \<L> Q'" "Q' \<preceq> Q"
  using assms 
proof (induct \<L> P "Basic \<alpha>" Q)
  case (basic \<L> P i Q)
  hence "act \<L> P i \<alpha> (P+[\<alpha>,i]\<^sub>\<L>)" "st \<L> (P+[\<alpha>,i]\<^sub>\<L>)" "P+[\<alpha>,i]\<^sub>\<L> \<preceq> Q" 
    by (auto simp: act_def wfs_def)
  then show ?case using basic(1,2,4) basic(3)[of P i "P+[\<alpha>,i]\<^sub>\<L>"] by auto
qed blast

lemma pre_stableE:
  assumes "\<L> \<turnstile> P {c} Q"
  obtains P' where "P \<preceq> P'" "st \<L> P'" "\<L> \<turnstile> P' {c} Q"
  using assms by (induct) blast+

section \<open>Rewrite Rules\<close>

lemma rwI:
  assumes "\<L> \<turnstile> P {c} Q"
  assumes "c \<leadsto> c'"
  shows "\<L> \<turnstile> P {c'} Q"
  using assms
proof (induct arbitrary: c')
  case (seq \<L> Q c\<^sub>2 M P c\<^sub>1)
  thus ?case by (cases rule: silentE[OF seq(5)]; auto)
next
  case (choice \<L> P c\<^sub>1 Q c\<^sub>2)
  thus ?case by (cases rule: silentE[OF choice(5)]; auto)
next
  case (loop \<L> P c)
  thus ?case by (cases rule: silentE[OF loop(4)]; auto)
next
  case (conseq \<L> P c Q P' Q')
  then show ?case by auto
qed (cases rule: silentE; auto)+

lemma multi_rwI:
  assumes "c \<mapsto>Nil\<^sup>* c'"
  assumes "\<L> \<turnstile> P {c} Q"
  shows "\<L> \<turnstile> P {c'} Q"
  using assms by (induct c "[] :: 'a list" c') (auto intro: rwI)

section \<open>Execution Rules\<close>

lemma reorder_inst:
  assumes wf: "wfs \<L>" 
  assumes a: "st \<L> P" "\<alpha>' < \<beta> <\<^sub>a \<alpha>" 
  assumes \<beta>: "act \<L> P i \<beta> M" 
  assumes \<alpha>: "exc \<L> M j \<alpha> Q"
  shows "\<exists>N. exc \<L> P j \<alpha>' N \<and> st \<L> N \<and> act \<L> N i \<beta> Q"
proof -
  have "use M \<alpha> \<subseteq> use (P+[\<beta>,i]\<^sub>\<L>) \<alpha>" using assms(4) by (auto simp: act_def state_ord_def)
  hence "vars \<alpha> \<subseteq> use (P+[\<beta>,i]\<^sub>\<L>) \<alpha>" using assms(5) by (auto simp: exc_def act_po_def)
  hence v: "sphere \<L> i \<beta> \<subseteq> - wr \<alpha>' \<inter> - rd \<alpha>'"
    using assms(3,5) refacts[OF assms(3)] by auto
  have \<alpha>': "act_po \<L> (P-[\<beta>,i]\<^sub>\<L>) j \<alpha>'" 
    using expi_act_po assms unfolding exc_def act_def by blast
  have w: "wk_check \<L> (P-[\<beta>,i]\<^sub>\<L>) j \<alpha>'" using \<alpha>' assms(1) 
      by (auto simp: act_po_def wfs_def)
  have t: "transitive (rely \<L>)" using assms(1) by (auto simp: wfs_def)

  have \<alpha>2: "act_po \<L> P j \<alpha>'" using \<alpha>' by auto
  have \<beta>2: "act_po \<L> (P\<oplus>[\<alpha>',j]\<^sub>\<L>) i \<beta>" using delay_act_po[OF a w v]
  proof -
    have "act_po \<L> P i \<beta>" using assms \<alpha>' by (auto simp: act_po_def act_def)
    thus ?thesis by (rule delay_act_po[OF assms(2,3) w v])
  qed
  have e: "(P\<oplus>[\<alpha>',j]\<^sub>\<L>)+[\<beta>,i]\<^sub>\<L> \<preceq> Q"
  proof -
    have "(P\<oplus>[\<alpha>',j]\<^sub>\<L>)+[\<beta>,i]\<^sub>\<L> \<preceq> (P+[\<beta>,i]\<^sub>\<L>)\<oplus>[\<alpha>,j]\<^sub>\<L>" using reorder_updates[OF assms(2,3) w t v] 
      by blast
    also have "... \<preceq> Q" using assms(4,5) unfolding act_def exc_def by blast
    finally show ?thesis .
  qed
  have "st \<L> (P\<oplus>[\<alpha>',j]\<^sub>\<L>)" using assms(1) by (auto simp: wfs_def)
  thus ?thesis using \<alpha>2 \<beta>2 e unfolding act_def exc_def by auto
qed

lemma reorder_com:
  assumes "wfs \<L>" "e' < c <\<^sub>c e" "\<L> \<turnstile> P {c} M" "exc \<L> M i e Q" "st \<L> Q"
  shows "\<exists>M. exc \<L> P i e' M \<and> \<L> \<turnstile> M {c} Q"
  using assms
proof (induct e' c e arbitrary: P M Q rule: reorder_com.induct)
  case (1 e' e)
  hence e: "exc \<L> P i e' Q" "st \<L> Q" by (auto intro: exc_conseq)
  then show ?case by blast
next
  case (2 e' \<alpha> e)
  obtain P' M' j where s: "P \<preceq> P'" "st \<L> P'" "act \<L> P' j \<alpha> M'" "st \<L> M'" "M' \<preceq> M" using 2(1,3) by auto
  hence "act \<L> P' j \<alpha> M" by (auto intro: act_conseq)
  hence "\<exists>N. exc \<L> P' i e' N \<and> st \<L> N \<and> act \<L> N j \<alpha> Q" using reorder_inst 2 s by auto
  then show ?case using s(1) by (blast intro: exc_conseq)
next
  case (3 e' c\<^sub>1 c\<^sub>2 e)
  obtain e\<^sub>n where e: "e' < c\<^sub>1 <\<^sub>c e\<^sub>n" "e\<^sub>n < c\<^sub>2 <\<^sub>c e" using 3(4) by auto
  obtain N where n: "\<L> \<turnstile> P {c\<^sub>1} N" "\<L> \<turnstile> N {c\<^sub>2} M" using 3(4,5) by (blast)
  obtain N' where n': "exc \<L> N i e\<^sub>n N'" "\<L> \<turnstile> N' {c\<^sub>2} Q" 
    using e n 3(2)[OF 3(3) e(2) n(2) 3(6,7)] by blast
  obtain N'' where s: "N' \<preceq> N''" "st \<L> N''" "\<L> \<turnstile> N'' {c\<^sub>2} Q" 
    using n'(2) by (blast elim: pre_stableE)
  have s': "exc \<L> N i e\<^sub>n N''" using n'(1) s(1) by (auto intro: exc_conseq)
  obtain M' where m': "exc \<L> P i e' M'" "\<L> \<turnstile> M' {c\<^sub>1} N''" using 3(1)[OF 3(3) e(1) n(1) s' s(2)] 
    by blast 
  then show ?case using s by blast
qed auto

lemma progI:
  assumes "c \<mapsto>[e',r,e] c'"
  assumes "wfs \<L>"
  assumes "\<L> \<turnstile> P {c} Q"
  shows "\<exists>M i. exc \<L> P i e' M \<and> \<L> \<turnstile> M {c'} Q"
  using assms
proof (induct arbitrary: P Q)
  case (act \<alpha>)
  then obtain P' Q' j where e: "P \<preceq> P'" "st \<L> P'" "act \<L> P' j \<alpha> Q'" "st \<L> Q'" "Q' \<preceq> Q" by blast
  hence "exc \<L> P j \<alpha> Q'" using act(2) by (auto intro: act_to_exec act_conseq)
  moreover have "\<L> \<turnstile> Q' {Skip} Q" using e(4,5) by blast
  ultimately show ?case by blast
next
  case (ino c\<^sub>1 e' r e c\<^sub>1' c\<^sub>2)
  then obtain M where m: "\<L> \<turnstile> P {c\<^sub>1} M" "\<L> \<turnstile> M {c\<^sub>2} Q" by auto
  then obtain N i where n: "exc \<L> P i e' N" "\<L> \<turnstile> N {c\<^sub>1'} M" using ino by blast
  then show ?case using m by auto
next
  case (ooo c\<^sub>2 e' r e c\<^sub>2' e'' c\<^sub>1)
  then obtain M where m: "\<L> \<turnstile> P {c\<^sub>1} M" "\<L> \<turnstile> M {c\<^sub>2} Q" by auto
  then obtain N i where n: "exc \<L> M i e' N" "\<L> \<turnstile> N {c\<^sub>2'} Q" using ooo by (blast)
  then obtain N' where n: "exc \<L> M i e' N'" "st \<L> N'" "\<L> \<turnstile> N' {c\<^sub>2'} Q" 
    by (blast intro: exc_conseq elim: pre_stableE)
  then show ?case using reorder_com[OF ooo(4,3) m(1) n(1,2)] by blast
qed 

lemma multi_progI:
  assumes "c \<mapsto>([e])\<^sup>* c'"
  assumes "wfs \<L>"
  assumes "\<L> \<turnstile> P {c} Q"
  shows "\<exists>M i. exc \<L> P i e M \<and> \<L> \<turnstile> M {c'} Q"
  using assms
proof (induct c "[e]" c' arbitrary: \<L> P Q)
  case (prepend c\<^sub>1 r e' c\<^sub>2 c\<^sub>3)
  obtain M i where "exc \<L> P i e M" "\<L> \<turnstile> M {c\<^sub>2} Q" 
    using progI[OF prepend(1,3,4)] by blast
  thus ?case using prepend(2) by (blast intro: multi_rwI)
qed (blast intro: rwI)

lemma info_type_progE:
  assumes "c \<mapsto>([\<alpha>])\<^sup>* c'"
  assumes "wfs \<L>"
  assumes "\<L> \<turnstile> P {c} Q"
  obtains M i where "exc \<L> P i \<alpha> M" "\<L> \<turnstile> M {c'} Q"
  using assms multi_progI by blast

end

section \<open>Security Property\<close>

context rules
begin

text \<open>The security bisimulation property we are enforcing\<close>
definition secure :: "('b,'c,'d) spec \<Rightarrow> ('a,'b,'c,'d) state \<Rightarrow> 'a com \<Rightarrow> bool"
  where "secure \<L> P c \<equiv> \<forall>m\<^sub>1 m\<^sub>2 c\<^sub>1 t m\<^sub>1'. 
          \<langle>c,m\<^sub>1\<rangle> \<rightarrow>t\<^sup>* \<langle>c\<^sub>1,m\<^sub>1'\<rangle> \<longrightarrow> m\<^sub>1 =\<^bsub>\<L>,mem P\<^esub> m\<^sub>2 \<longrightarrow> 
          ((\<exists>m\<^sub>2' c\<^sub>2. \<langle>c,m\<^sub>2\<rangle> \<rightarrow>t\<^sup>* \<langle>c\<^sub>2,m\<^sub>2'\<rangle>) \<and>
          (\<forall>m\<^sub>2' c\<^sub>2. \<langle>c,m\<^sub>2\<rangle> \<rightarrow>t\<^sup>* \<langle>c\<^sub>2,m\<^sub>2'\<rangle> \<longrightarrow> m\<^sub>1' =\<^bsub>\<L>,UNIV\<^esub> m\<^sub>2'))" 

section \<open>Bisimulation\<close>

lemma preserve_low_eq:
  assumes "(m\<^sub>1, m\<^sub>1') \<in> eval \<alpha>"
  assumes "(m\<^sub>2, m\<^sub>2') \<in> eval \<alpha>"
  assumes "m\<^sub>1 =\<^bsub>\<L>,mem P\<^esub> m\<^sub>2"
  assumes "exc \<L> P i \<alpha> Q"
  shows "m\<^sub>1' =\<^bsub>\<L>,mem Q\<^esub> m\<^sub>2'"
proof -
  have s: "\<lfloor>pred P i \<alpha>\<rfloor> \<inter> beh \<L> \<alpha> \<subseteq> guarantee \<L>" 
    using assms(4) by (auto simp: act_po_def exc_def)

  obtain \<Gamma>\<^sub>1 where g1: "m\<^sub>1 \<Otimes> \<Gamma>\<^sub>1 \<in> pol \<L>" "m\<^sub>1 \<Otimes> \<Gamma>\<^sub>1 \<in> mem P" "\<L> \<turnstile> m\<^sub>1 =\<^bsub>\<Gamma>\<^sub>1\<^esub> m\<^sub>2" 
    using assms(3) by (auto simp: low_equiv_def)
  obtain \<Gamma>\<^sub>1' where g1': "(\<Gamma>\<^sub>1,\<Gamma>\<^sub>1') \<in> flow \<alpha> (vis \<L>)"
    using flow_def[of \<alpha> "vis \<L>"] apply auto
    by (metis (no_types, lifting) eq_fst_iff fst_conv imageE rangeI range_fst)

  obtain \<Gamma>\<^sub>2 where g2: "m\<^sub>2 \<Otimes> \<Gamma>\<^sub>2 \<in> pol \<L>" "m\<^sub>2 \<Otimes> \<Gamma>\<^sub>2 \<in> mem P" "\<L> \<turnstile> m\<^sub>1 =\<^bsub>\<Gamma>\<^sub>2\<^esub> m\<^sub>2" 
    using assms(3) by (auto simp: low_equiv_def)
  obtain \<Gamma>\<^sub>2' where g2': "(\<Gamma>\<^sub>2,\<Gamma>\<^sub>2') \<in> flow \<alpha> (vis \<L>)"
    using flow_def[of \<alpha> "vis \<L>"] apply auto
    by (metis (no_types, lifting) eq_fst_iff fst_conv imageE rangeI range_fst)

  have b: "(m\<^sub>1 \<Otimes> \<Gamma>\<^sub>1, m\<^sub>1' \<Otimes> \<Gamma>\<^sub>1') \<in> beh \<L> \<alpha>"
    using assms(1) g1' by (auto simp: beh_def)
  hence "m\<^sub>1' \<Otimes> \<Gamma>\<^sub>1' \<in> mem (P\<oplus>[\<alpha>,i]\<^sub>\<L>)" 
    using g1(2) unfolding ord_def sp\<^sub>E_def by auto
  hence q1: "m\<^sub>1' \<Otimes> \<Gamma>\<^sub>1' \<in> mem Q" 
    using assms(4) unfolding exc_def state_ord_def by auto
  have "(m\<^sub>1 \<Otimes> \<Gamma>\<^sub>1, m\<^sub>1' \<Otimes> \<Gamma>\<^sub>1') \<in> guarantee \<L>" 
    using s b g1(2) by (auto simp: pre_def restrict_def)
  hence p1: "m\<^sub>1' \<Otimes> \<Gamma>\<^sub>1' \<in> pol \<L>" 
    using g1 unfolding pol_def guarantee_def by auto

  have b: "(m\<^sub>2 \<Otimes> \<Gamma>\<^sub>2, m\<^sub>2' \<Otimes> \<Gamma>\<^sub>2') \<in> beh \<L> \<alpha>"
    using assms(2) g2' by (auto simp: beh_def)
  hence "m\<^sub>2' \<Otimes> \<Gamma>\<^sub>2' \<in> mem (P\<oplus>[\<alpha>,i]\<^sub>\<L>)" 
    using g2(2) unfolding ord_def sp\<^sub>E_def by auto
  hence q2: "m\<^sub>2' \<Otimes> \<Gamma>\<^sub>2' \<in> mem Q" 
    using assms(4) unfolding exc_def state_ord_def by auto
  have "(m\<^sub>2 \<Otimes> \<Gamma>\<^sub>2, m\<^sub>2' \<Otimes> \<Gamma>\<^sub>2') \<in> guarantee \<L>" 
    using s b g2(2) by (auto simp: pre_def restrict_def)
  hence p2: "m\<^sub>2' \<Otimes> \<Gamma>\<^sub>2' \<in> pol \<L>" 
    using g2 unfolding pol_def guarantee_def by auto

  have "m\<^sub>1' \<Otimes> \<Gamma>\<^sub>1' \<in> pol \<L> \<inter> mem Q" using p1 q1 by auto
  moreover have "m\<^sub>2' \<Otimes> \<Gamma>\<^sub>2' \<in> pol \<L> \<inter> mem Q" using p2 q2 by auto
  moreover have"\<L> \<turnstile> m\<^sub>1' =\<^bsub>\<Gamma>\<^sub>1'\<^esub> m\<^sub>2'" 
    using g1 flow_sound[OF assms(1,2) g1'] by auto
  moreover have "\<L> \<turnstile> m\<^sub>1' =\<^bsub>\<Gamma>\<^sub>2'\<^esub> m\<^sub>2'" 
    using base_equiv_sym flow_sound[OF assms(2,1) g2' base_equiv_sym[OF g2(3)]] by auto
  ultimately show ?thesis unfolding low_equiv_def by blast
qed

lemma exec_secure:
  assumes "exc \<L> P i \<alpha> Q"
  assumes "m\<^sub>1 =\<^bsub>\<L>,mem P\<^esub> m\<^sub>2"
  assumes "(m\<^sub>1, m\<^sub>1') \<in> eval \<alpha>"
  obtains m\<^sub>2' where "(m\<^sub>2, m\<^sub>2') \<in> eval \<alpha>" "m\<^sub>1' =\<^bsub>\<L>,mem Q\<^esub> m\<^sub>2'"
proof -
  have "m\<^sub>1 =\<^bsub>\<L>,pred P i \<alpha>\<^esub> m\<^sub>2" using assms(2) by (rule low_equiv_subI) (auto simp: restrict_def)
  then obtain m\<^sub>2' where "(m\<^sub>2, m\<^sub>2') \<in> eval \<alpha>"
    using assms(1,3) unfolding progress_def exc_def act_po_def
    by meson
  moreover have "m\<^sub>1' =\<^bsub>\<L>,mem Q\<^esub> m\<^sub>2'"
    using calculation assms(1,2,3) preserve_low_eq by auto
  ultimately show ?thesis using that by blast
qed

lemma bisim_exists:
  assumes "\<L> \<turnstile> P {c} Q"
  assumes "wfs \<L>"
  assumes "m\<^sub>1 =\<^bsub>\<L>,mem P\<^esub> m\<^sub>2"
  assumes "\<langle>c,m\<^sub>1\<rangle> \<rightarrow>t\<^sup>* \<langle>c\<^sub>1,m\<^sub>1'\<rangle>"
  obtains c\<^sub>2 m\<^sub>2' where "\<langle>c,m\<^sub>2\<rangle> \<rightarrow>t\<^sup>* \<langle>c\<^sub>2,m\<^sub>2'\<rangle>" "m\<^sub>1' =\<^bsub>\<L>,UNIV\<^esub> m\<^sub>2'"
  using assms unfolding ev_def
proof safe
  assume "trace_mem m\<^sub>1 t m\<^sub>1'" "wfs \<L>" "c \<mapsto>t\<^sup>* c\<^sub>1" "\<L> \<turnstile> P {c} Q" "m\<^sub>1 =\<^bsub>\<L>,mem P\<^esub> m\<^sub>2"
  hence "\<exists>m\<^sub>2' c\<^sub>2. (trace_mem m\<^sub>2 t m\<^sub>2' \<and> c \<mapsto>t\<^sup>* c\<^sub>2) \<and> m\<^sub>1' =\<^bsub>\<L>,UNIV\<^esub> m\<^sub>2'"
  proof (induct m\<^sub>1 t m\<^sub>1' arbitrary: P m\<^sub>2 c rule: trace_mem.induct)
    case (1 m)
    then show ?case using low_equiv_subI by blast
  next
    case (2 m\<^sub>1 m\<^sub>1' \<alpha> t m\<^sub>1'')
    then obtain c' where f1: "c \<mapsto>([\<alpha>])\<^sup>* c'" "c' \<mapsto>t\<^sup>* c\<^sub>1" by auto
    obtain M i where f2: "exc \<L> P i \<alpha> M" "\<L> \<turnstile> M {c'} Q"
      using info_type_progE[OF f1(1) 2(4,6)] by metis
    then obtain m\<^sub>2' where f3: "(m\<^sub>2,m\<^sub>2') \<in> eval \<alpha>" "m\<^sub>1' =\<^bsub>\<L>,mem M\<^esub> m\<^sub>2'"
      using 2(1,7) exec_secure[OF f2(1) 2(7,1)] by blast
    hence "\<exists>m\<^sub>2'' c\<^sub>2. (trace_mem m\<^sub>2' t m\<^sub>2'' \<and> c' \<mapsto>t\<^sup>* c\<^sub>2) \<and> m\<^sub>1'' =\<^bsub>\<L>,UNIV\<^esub> m\<^sub>2''" 
      using 2(3)[OF 2(4) f1(2) f2(2)] by auto
    then show ?case using f3(1) 2(5) by blast
  qed
  thus ?thesis using that unfolding ev_def by auto
qed

theorem secure_bisim:
  assumes "\<L> \<turnstile> P { c } Q" "wfs \<L>"
  shows "secure \<L> P c"
  using bisim_exists[OF assms] ev_det
  unfolding secure_def by metis

end

end