theory Security
  imports State Reordering
begin

chapter \<open>Security\<close>

section \<open>Security Concept Embedding\<close>

text \<open>
We model variable classifications as an extension to the value encoding.
\<close>

type_synonym ('r,'v,'s) smem = "'r \<Rightarrow> ('v \<times> 's)"
type_synonym ('r,'s) styp = "'r \<Rightarrow> 's"
type_synonym ('r,'v,'s) spred = "('r,'v,'s) smem set"
type_synonym ('r,'v,'s) srel = "('r,'v,'s) smem rel"

section \<open>Security Properties\<close>

abbreviation \<Gamma> where "\<Gamma> m \<equiv> \<lambda>x. snd (m x)"
abbreviation \<A> where "\<A> m \<equiv> \<lambda>x. fst (m x)"
abbreviation merge (infix "\<Otimes>" 60) where "merge m \<gamma> \<equiv> \<lambda>x. (m x, \<gamma> x)"

subsection \<open>Definitions\<close>

text \<open>Convert a policy into a state that asserts it\<close>
definition pol :: "('b,'c,'d::bounded_lattice) spec \<Rightarrow> ('b,'c,'d) spred"
  where "pol \<L> \<equiv> {m. \<forall>x. sec \<L> (\<A> m) x \<ge> \<Gamma> m x}"

text \<open>Convert a policy into a step that preserves it\<close>
definition rpol :: "('b,'c,'d::bounded_lattice) spec \<Rightarrow> ('b,'c,'d) srel"
  where "rpol \<L> \<equiv> preserve (pol \<L>)"

text \<open>Describe low equivalence between two memories for one \<gamma>\<close>
definition base_equiv :: "('b,'c,'d::bounded_lattice) spec \<Rightarrow> ('b \<Rightarrow> 'c) \<Rightarrow> (_,_) styp \<Rightarrow> ('b \<Rightarrow> 'c) \<Rightarrow> bool"
  ("_ \<turnstile> _ =\<^bsub>_\<^esub> _" [70,0,0,70] 100)
  where "\<L> \<turnstile> m\<^sub>1 =\<^bsub>\<gamma>\<^esub> m\<^sub>2 \<equiv> \<forall>x. (\<gamma> x \<le> vis \<L> \<or> \<gamma> x \<le> vis \<L>) \<longrightarrow> m\<^sub>1 x = m\<^sub>2 x"

text \<open>Describe low equivalence between two memories for an analysis state\<close>
definition low_equiv :: "('b \<Rightarrow> 'c) \<Rightarrow> ('b,'c,'d::bounded_lattice) spec \<Rightarrow> ('b,'c,'d) pred \<Rightarrow> ('b \<Rightarrow> 'c) \<Rightarrow> bool"
  ("_ =\<^bsub>_,_\<^esub> _" [70,0,0,70] 100)
  where "m\<^sub>1 =\<^bsub>\<L>,P\<^esub> m\<^sub>2 \<equiv> \<exists>\<Gamma>\<^sub>1 \<Gamma>\<^sub>2. m\<^sub>1 \<Otimes> \<Gamma>\<^sub>1 \<in> pol \<L> \<inter> P \<and> m\<^sub>2 \<Otimes> \<Gamma>\<^sub>2 \<in> pol \<L> \<inter> P \<and> \<L> \<turnstile> m\<^sub>1 =\<^bsub>\<Gamma>\<^sub>1\<^esub> m\<^sub>2 \<and> \<L> \<turnstile> m\<^sub>1 =\<^bsub>\<Gamma>\<^sub>2\<^esub> m\<^sub>2"

text \<open>Low equivalence is symmetric\<close>
lemma base_equiv_sym [sym]:
  "\<L> \<turnstile> m\<^sub>1 =\<^bsub>\<gamma>\<^esub> m\<^sub>2 \<Longrightarrow> \<L> \<turnstile> m\<^sub>2 =\<^bsub>\<gamma>\<^esub> m\<^sub>1"
  unfolding base_equiv_def by auto

theorem low_equiv_sym [sym]:
  "m\<^sub>1 =\<^bsub>\<L>,P\<^esub> m\<^sub>2 \<Longrightarrow> m\<^sub>2 =\<^bsub>\<L>,P\<^esub> m\<^sub>1"
  unfolding low_equiv_def using base_equiv_sym by blast

text \<open>Low equivalence on a stronger state implies low equivalence on a weaker\<close>
lemma low_equiv_subI [intro]:
  "m\<^sub>1 =\<^bsub>\<L>,P\<^esub> m\<^sub>2 \<Longrightarrow> P \<subseteq> Q \<Longrightarrow> m\<^sub>1 =\<^bsub>\<L>,Q\<^esub> m\<^sub>2"
proof -
  assume a: "P \<subseteq> Q" "m\<^sub>1 =\<^bsub>\<L>,P\<^esub> m\<^sub>2"
  hence "pol \<L> \<inter> P \<subseteq> pol \<L> \<inter> Q" by auto
  thus ?thesis using a unfolding low_equiv_def by blast
qed

end
